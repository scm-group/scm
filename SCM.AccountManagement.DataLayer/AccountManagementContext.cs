﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SCM.AccountManagement.Model;

namespace SCM.AccountManagement.DataLayer
{
    //public class AccountManagementContext : IdentityDbContext
    public class AccountManagementContext : IdentityDbContext<TblUser>
    {
        public AccountManagementContext(DbContextOptions<AccountManagementContext> options) : base(options)
        {
        }

        //protected override void OnModelCreating(ModelBuilder builder)
        //{
        //    base.OnModelCreating(builder);
        //}

        public DbSet<TblUser> TblUsers { get; set; }
    }
}
