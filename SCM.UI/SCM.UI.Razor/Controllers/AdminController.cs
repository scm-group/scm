﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SCM.AccountManagement.Model;
using SCM.Infrastructure.Utility;

namespace SCM.UI.Razor.Controllers
{
    //[Authorize(Roles = "Admin")]
    //[Authorize(Roles = "Admin, User")]
    public class AdminController : Controller
    {
        public IActionResult Index(string message)
        {
            //if (!User.Identity.IsAuthenticated)
            //    return RedirectToAction(nameof(Login), nameof(AccountController).GetControllerName(), new
            //    {
            //        message = "جهت ورود به حساب کاربری، مشخصات خود را وارد کنید"
            //    });

            if (message != null)
                ViewBag.Message = new string[] { message, message.Contains("خطا") ? "alert-danger" : "alert-success" };

            if (TempData["ModelState"] != null)
                ViewBag.ModelState = TempData["ModelState"] as IEnumerable<string>;

            return View();
        }

        public async Task<IActionResult> ShowUsers()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:44393/Account/");

            List<TblUser> listUser = new List<TblUser>();

            try
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var message = new HttpRequestMessage(HttpMethod.Post, "GetAllUser");
                message.Headers.Accept.Clear();
                //message.Content = new StringContent(), Encoding.UTF8, "application/json");

                await client.SendAsync(message).ContinueWith(async f =>
                {
                    //BadRequest
                    if (f.IsFaulted || !f.Result.IsSuccessStatusCode)
                    {
                        ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                    }
                    //OK
                    else
                    {
                        try
                        {
                            if (f.Result != null && f.Result.Content != null)
                            {
                                var message = await f.Result.Content.ReadAsStringAsync();
                                listUser = JsonConvert.DeserializeObject<List<TblUser>>(message);
                            }
                            else
                            {
                                ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("An error occurred when connect to api. Message = {0}", ex.Message);
                            Console.ForegroundColor = ConsoleColor.Gray;

                            ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("An error occurred when connect to api. Message = {0}", ex.Message);
                Console.ForegroundColor = ConsoleColor.Gray;

                ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
            }

            if (!listUser.Any())
                ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");

            if (!ModelState.IsValid)
            {
                TempData["ModelState"] = ModelState.GetErrors().Split(new char[] { '|' });
                return RedirectToAction(nameof(Index), nameof(AdminController).GetControllerName());
            }

            return View(listUser);
        }
    }
}