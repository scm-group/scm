﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SCM.AccountManagement.Core;
using SCM.AccountManagement.Model;
using SCM.AccountManagement.Model.ViewModels.Account;
using SCM.Infrastructure.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using RestSharp;
using ServiceStack;
using RestSharp.Deserializers;
using SCM.UI.Razor.Models;
using Microsoft.AspNetCore.Authorization;

namespace SCM.UI.Razor.Controllers
{
    public class AccountController : Controller
    {
        IAccountManagement _accountManagement;
        UserManager<TblUser> _userManager;
        SignInManager<TblUser> _signInManager;

        public AccountController(IAccountManagement accountManagement, UserManager<TblUser> userManager, SignInManager<TblUser> signInManager)
        {
            _accountManagement = accountManagement;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        //[Authorize]
        public IActionResult Index(string message)
        {
            //if (!_signInManager.IsSignedIn(User))
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction(nameof(Login), nameof(AccountController).GetControllerName(), new
                {
                    message = "جهت ورود به حساب کاربری، مشخصات خود را وارد کنید"
                });

            //if (User.Identity.IsAuthenticated)
            if (_signInManager.IsSignedIn(User))
            {
                if (User.IsInRole("Admin"))
                {
                    return RedirectToAction(nameof(Index), nameof(AdminController).GetControllerName(), new
                    {
                        message = "به پنل مدیریت مدیران خوش آمدید"
                    });
                }
                else if (User.IsInRole("User"))
                {
                    return RedirectToAction(nameof(Index), nameof(UserController).GetControllerName(), new
                    {
                        message = "به پنل مدیریت کاربران خوش آمدید"
                    });
                }
            }

            if (message != null)
                ViewBag.Message = new string[] { message, message.Contains("خطا") ? "alert-danger" : "alert-success" };

            return View();
        }

        public IActionResult Login(string message)
        {
            if (_signInManager.IsSignedIn(User))
            //if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole("Admin"))
                {
                    return RedirectToAction(nameof(Index), nameof(AdminController).GetControllerName(), new
                    {
                        message = "به پنل مدیریت مدیران خوش آمدید"
                    });
                }
                else if (User.IsInRole("User"))
                {
                    return RedirectToAction(nameof(Index), nameof(UserController).GetControllerName(), new
                    {
                        message = "به پنل مدیریت کاربران خوش آمدید"
                    });
                }

                return RedirectToAction(nameof(Index), nameof(AccountController).GetControllerName(), new
                {
                    message = "خوش آمدید"
                });
            }

            if (message != null)
                ViewBag.Message = new string[] { message, message.Contains("خطا") ? "alert-danger" : "alert-success" };

            //if (TempData["ModelState"] != null)
            //    ViewBag.ModelState = TempData["ModelState"] as IEnumerable<string>;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            if (!ModelState.IsValid)
            {
                //TempData["ModelState"] = ModelState.GetErrors().Split(new char[] { '|' });
                //return RedirectToAction(nameof(Login), nameof(AccountController).GetControllerName());

                //return RedirectToAction(nameof(Login), nameof(AccountController).GetControllerName(), new {
                //    message = "خطا: " + ModelState.GetErrors()
                //});

                ModelState.AddModelError(string.Empty, ModelState.GetErrors());
                return View(loginViewModel);
            }

            var client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:44393/Account/");

            //bool isExsit = _accountManagement.IsExsitPhoneNumber(loginViewModel.PhoneNumber);
            //if (isExsit == false)
            //{
            //    return RedirectToAction(nameof(Login), nameof(AccountController).GetControllerName(), new
            //    {
            //        message = "موبایل معتبر نمی باشد"
            //    });
            //}
            try
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var message = new HttpRequestMessage(HttpMethod.Post, "CheckPhoneNumber");
                message.Headers.Accept.Clear();
                message.Content = new StringContent(JsonConvert.SerializeObject(new {
                    PhoneNumber = loginViewModel.PhoneNumber
                }), Encoding.UTF8, "application/json");

                await client.SendAsync(message).ContinueWith(async f =>
                {
                    //BadRequest
                    if (f.IsFaulted || !f.Result.IsSuccessStatusCode)
                    {
                        ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                    }
                    //OK
                    else
                    {
                        try
                        {
                            if (f.Result != null && f.Result.Content != null)
                            {
                                var message = await f.Result.Content.ReadAsStringAsync();
                                var stringMessage = JsonConvert.DeserializeObject<ResponseMessageModel>(message);

                                if (stringMessage.Status == false)
                                    ModelState.AddModelError(string.Empty, stringMessage.Message);
                            }
                            else
                                ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                        }
                        catch (Exception ex)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("An error occurred when connect to api. Message = {0}", ex.Message);
                            Console.ForegroundColor = ConsoleColor.Gray;

                            ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                        }
                    }

                    //if (f.IsFaulted || !f.Result.IsSuccessStatusCode)
                    //{
                    //    try
                    //    {
                    //        if (f.Result != null && f.Result.Content != null)
                    //        {
                    //            var message = await f.Result.Content.ReadAsStringAsync();
                    //            var stringMessage = JsonConvert.DeserializeObject<ResponseMessageModel>(message);

                    //            //ModelState.AddModelError(nameof(loginViewModel.PhoneNumber), stringMessage.Message);
                    //            ModelState.AddModelError(string.Empty, stringMessage.Message);
                    //        }
                    //        else
                    //        {
                    //            ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        Console.ForegroundColor = ConsoleColor.Red;
                    //        Console.WriteLine("An error occurred when connect to api. Message = {0}", ex.Message);
                    //        Console.ForegroundColor = ConsoleColor.Gray;

                    //        ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                    //    }
                    //}
                });
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("An error occurred when connect to api. Message = {0}", ex.Message);
                Console.ForegroundColor = ConsoleColor.Gray;

                ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
            }

            //var user = _accountManagement.GetUserByPhoneNumberPassword(loginViewModel.PhoneNumber, loginViewModel.Password);
            //if (user == null)
            //{
            //    return RedirectToAction(nameof(Login), nameof(AccountController).GetControllerName(), new
            //    {
            //        message = "کاربری با این مشخصات موجود نمی باشد"
            //    });
            //}
            try
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var message = new HttpRequestMessage(HttpMethod.Post, "CheckPhoneNumberPassword");
                message.Headers.Accept.Clear();
                message.Content = new StringContent(JsonConvert.SerializeObject(new
                {
                    PhoneNumber = loginViewModel.PhoneNumber,
                    Password = loginViewModel.Password
                }), Encoding.UTF8, "application/json");

                await client.SendAsync(message).ContinueWith(async f =>
                {
                    //BadRequest
                    if (f.IsFaulted || !f.Result.IsSuccessStatusCode)
                    {
                        try
                        {
                            if (f.Result != null && f.Result.Content != null)
                            {
                                var message = await f.Result.Content.ReadAsStringAsync();
                                var stringMessage = JsonConvert.DeserializeObject<ResponseMessageModel>(message);

                                ModelState.AddModelError(string.Empty, stringMessage.Message);
                            }
                            else
                            {
                                ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("An error occurred when connect to api. Message = {0}", ex.Message);
                            Console.ForegroundColor = ConsoleColor.Gray;

                            ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("An error occurred when connect to api. Message = {0}", ex.Message);
                Console.ForegroundColor = ConsoleColor.Gray;

                ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
            }

            //if (!user.IsActive)
            //{
            //    return RedirectToAction(nameof(Login), nameof(AccountController).GetControllerName(), new
            //    {
            //        message = "حساب کاربری غیر فعال می باشد"
            //    });
            //}
            /*
            try
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var message = new HttpRequestMessage(HttpMethod.Post, "CheckUserIsActive");
                message.Headers.Accept.Clear();
                message.Content = new StringContent(JsonConvert.SerializeObject(new
                {
                    PhoneNumber = loginViewModel.PhoneNumber
                }), Encoding.UTF8, "application/json");

                await client.SendAsync(message).ContinueWith(async f =>
                {
                    //BadRequest
                    if (f.IsFaulted || !f.Result.IsSuccessStatusCode)
                    {
                        try
                        {
                            if (f.Result != null && f.Result.Content != null)
                            {
                                var message = await f.Result.Content.ReadAsStringAsync();
                                var stringMessage = JsonConvert.DeserializeObject<ResponseMessageModel>(message);

                                ModelState.AddModelError(string.Empty, stringMessage.Message);
                            }
                            else
                            {
                                ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("An error occurred when connect to api. Message = {0}", ex.Message);
                            Console.ForegroundColor = ConsoleColor.Gray;

                            ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("An error occurred when connect to api. Message = {0}", ex.Message);
                Console.ForegroundColor = ConsoleColor.Gray;

                ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
            }
            */

            TblUser tblUser = new TblUser();

            try
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var message = new HttpRequestMessage(HttpMethod.Post, "GetUserByPhoneNumber");
                message.Headers.Accept.Clear();
                message.Content = new StringContent(JsonConvert.SerializeObject(new
                {
                    PhoneNumber = loginViewModel.PhoneNumber
                }), Encoding.UTF8, "application/json");

                await client.SendAsync(message).ContinueWith(async f =>
                {
                    //BadRequest
                    if (f.IsFaulted || !f.Result.IsSuccessStatusCode)
                    {
                        ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                    }
                    //OK
                    else
                    {
                        try
                        {
                            if (f.Result != null && f.Result.Content != null)
                            {
                                var message = await f.Result.Content.ReadAsStringAsync();
                                tblUser = JsonConvert.DeserializeObject<TblUser>(message);
                            }
                            else
                            {
                                ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("An error occurred when connect to api. Message = {0}", ex.Message);
                            Console.ForegroundColor = ConsoleColor.Gray;

                            ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("An error occurred when connect to api. Message = {0}", ex.Message);
                Console.ForegroundColor = ConsoleColor.Gray;

                ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
            }

            if (tblUser == null)
                ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");

            if (!ModelState.IsValid)
                return View(loginViewModel);

            //await _signInManager.PasswordSignInAsync(tblUser.UserName, loginViewModel.Password, loginViewModel.RememberMe, true);
            var result = await _signInManager.PasswordSignInAsync("hamidfathi66@gmail.com", "123456aA@", true, true);

            //var claim = new List<Claim>()
            //{
            //    new Claim(ClaimTypes.NameIdentifier, tblUser.Id.ToString()),
            //    new Claim(ClaimTypes.Name, tblUser.FirstName + " " + tblUser.LastName),
            //    new Claim(ClaimTypes.MobilePhone, tblUser.PhoneNumber),
            //    new Claim(ClaimTypes.Email, tblUser.Email),
            //    //new Claim(ClaimTypes.Role, tblUser.UserRoles.FirstOrDefault().Role.Name)
            //};
            //var identity = new ClaimsIdentity(claim, CookieAuthenticationDefaults.AuthenticationScheme);
            //var principal = new ClaimsPrincipal(identity);
            //var properties = new AuthenticationProperties
            //{
            //    IsPersistent = (bool)loginViewModel.RememberMe
            //};
            //await HttpContext.SignInAsync(principal, properties);

            //if (User.Identity.IsAuthenticated)
            if (_signInManager.IsSignedIn(User))
            {
                if (User.IsInRole("Admin"))
                {
                    return RedirectToAction(nameof(Index), nameof(AdminController).GetControllerName(), new
                    {
                        message = "به پنل مدیریت مدیران خوش آمدید"
                    });
                }
                else if (User.IsInRole("User"))
                {
                    return RedirectToAction(nameof(Index), nameof(UserController).GetControllerName(), new
                    {
                        message = "به پنل مدیریت کاربران خوش آمدید"
                    });
                }
            }

            //ViewBag.IsSuccess = true;
            //return View();
            return RedirectToAction(nameof(Index), nameof(AccountController).GetControllerName(), new
            {
                message = "خوش آمدید"
            });
        }

        public IActionResult Register(string message)
        {
            if (_signInManager.IsSignedIn(User))
            //if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole("Admin"))
                {
                    return RedirectToAction(nameof(Index), nameof(AdminController).GetControllerName(), new
                    {
                        message = "به پنل مدیریت مدیران خوش آمدید"
                    });
                }
                else if (User.IsInRole("User"))
                {
                    return RedirectToAction(nameof(Index), nameof(UserController).GetControllerName(), new
                    {
                        message = "به پنل مدیریت کاربران خوش آمدید"
                    });
                }

                return RedirectToAction(nameof(Index), nameof(AccountController).GetControllerName(), new
                {
                    message = "خوش آمدید"
                });
            }

            if (message != null)
                ViewBag.Message = new string[] { message, message.Contains("خطا") ? "alert-danger" : "alert-success" };

            if (TempData["ModelState"] != null)
                ViewBag.ModelState = TempData["ModelState"] as IEnumerable<string>;

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> RegisterLegalUser(RegisterLegalUserViewModel registerLegalUserViewModel)
        {
            if (!ModelState.IsValid)
            {
                TempData["ModelState"] = ModelState.GetErrors().Split(new char[] { '|' });
                return RedirectToAction(nameof(Register), nameof(AccountController).GetControllerName());

                //return RedirectToAction(nameof(Register), nameof(AccountController).GetControllerName(), new { message = "خطا: " + ModelState.GetErrors() });

                //ModelState.AddModelError(string.Empty, ModelState.GetErrors());
                //return View(registerLegalUserViewModel);
            }

            var client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:44393/Account/");

            //if (_accountManagement.IsExsitPhoneNumber(registerLegalUserViewModel.PhoneNumber))
            //{
            //    return RedirectToAction(nameof(Register), nameof(AccountController).GetControllerName(), new
            //    {
            //        message = "موبایل تکراری است"
            //    });
            //}
            try
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var message = new HttpRequestMessage(HttpMethod.Post, "CheckPhoneNumber");
                message.Headers.Accept.Clear();
                message.Content = new StringContent(JsonConvert.SerializeObject(new
                {
                    PhoneNumber = registerLegalUserViewModel.PhoneNumber
                }), Encoding.UTF8, "application/json");

                await client.SendAsync(message).ContinueWith(async f =>
                {
                    //BadRequest
                    if (f.IsFaulted || !f.Result.IsSuccessStatusCode)
                    {
                        ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                    }
                    //OK
                    else
                    {
                        try
                        {
                            if (f.Result != null && f.Result.Content != null)
                            {
                                var message = await f.Result.Content.ReadAsStringAsync();
                                var stringMessage = JsonConvert.DeserializeObject<ResponseMessageModel>(message);

                                if (stringMessage.Status == true)
                                    ModelState.AddModelError(string.Empty, stringMessage.Message);
                            }
                            else
                                ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                        }
                        catch (Exception ex)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("An error occurred when connect to api. Message = {0}", ex.Message);
                            Console.ForegroundColor = ConsoleColor.Gray;

                            ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("An error occurred when connect to api. Message = {0}", ex.Message);
                Console.ForegroundColor = ConsoleColor.Gray;

                ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
            }

            //if (_accountManagement.IsExsitEmail(FixedText.FixedEmail(registerLegalUserViewModel.Email)))
            //{
            //    return RedirectToAction(nameof(Register), nameof(AccountController).GetControllerName(), new
            //    {
            //        message = "ایمیل تکراری است"
            //    });
            //}
            try
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var message = new HttpRequestMessage(HttpMethod.Post, "CheckEmail");
                message.Headers.Accept.Clear();
                message.Content = new StringContent(JsonConvert.SerializeObject(new
                {
                    Email = registerLegalUserViewModel.Email
                }), Encoding.UTF8, "application/json");

                await client.SendAsync(message).ContinueWith(async f =>
                {
                    //BadRequest
                    if (f.IsFaulted || !f.Result.IsSuccessStatusCode)
                    {
                        ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                    }
                    //OK
                    else
                    {
                        try
                        {
                            if (f.Result != null && f.Result.Content != null)
                            {
                                var message = await f.Result.Content.ReadAsStringAsync();
                                var stringMessage = JsonConvert.DeserializeObject<ResponseMessageModel>(message);

                                if (stringMessage.Status == true)
                                    ModelState.AddModelError(string.Empty, stringMessage.Message);
                            }
                            else
                                ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                        }
                        catch (Exception ex)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("An error occurred when connect to api. Message = {0}", ex.Message);
                            Console.ForegroundColor = ConsoleColor.Gray;

                            ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("An error occurred when connect to api. Message = {0}", ex.Message);
                Console.ForegroundColor = ConsoleColor.Gray;

                ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
            }

            /*
            if (_accountManagement.IsExsitRegistrationNumber(registerLegalUserViewModel.RegistrationNumber))
            {
                return RedirectToAction(nameof(Register), nameof(AccountController).GetControllerName(), new
                {
                    message = "شماره ثبت تکراری است"
                });
            }

            if (_accountManagement.IsExsitNationalId(registerLegalUserViewModel.NationalId))
            {
                return RedirectToAction(nameof(Register), "Account", new
                {
                    message = "شناسه ملی تکراری است"
                });
            }

            if (_accountManagement.IsExsitEconomicCode(registerLegalUserViewModel.EconomicCode))
            {
                return RedirectToAction(nameof(Register), nameof(AccountController).GetControllerName(), new
                {
                    message = "کد اقتصادی تکراری است"
                });
            }
            */

            //TblUser user = _accountManagement.AddLegalUser(registerLegalUserViewModel);
            //if (user == null)
            //    return RedirectToAction(nameof(Register), nameof(AccountController).GetControllerName(), new
            //    {
            //        message = "خطا: عملیات ثبت نام با شکست مواجه شد. لطفا مجددا تلاش بفرمایید"
            //    });
            try
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var message = new HttpRequestMessage(HttpMethod.Post, "InsertLegalUser");
                message.Headers.Accept.Clear();
                message.Content = new StringContent(JsonConvert.SerializeObject(new
                {
                    //RegisterLegalUserViewModel = registerLegalUserViewModel
                    FirstName = registerLegalUserViewModel.FirstName,
                    LastName = registerLegalUserViewModel.LastName,
                    PhoneNumber = registerLegalUserViewModel.PhoneNumber,
                    Email = registerLegalUserViewModel.Email,
                    Password = registerLegalUserViewModel.Password,
                    //ImageFile = registerLegalUserViewModel.ImageFile,
                    /*
                    RegistrationNumber = registerLegalUserViewModel.RegistrationNumber,
                    RegistrationPlace = registerLegalUserViewModel.RegistrationPlace,
                    RegistrationDate = registerLegalUserViewModel.RegistrationDate,
                    NationalId = registerLegalUserViewModel.NationalId,
                    EconomicCode = registerLegalUserViewModel.EconomicCode,
                    Address = registerLegalUserViewModel.Address
                    */
                }), Encoding.UTF8, "application/json");

                await client.SendAsync(message).ContinueWith(async f =>
                {
                    //BadRequest
                    if (f.IsFaulted || !f.Result.IsSuccessStatusCode)
                    {
                        try
                        {
                            if (f.Result != null && f.Result.Content != null)
                            {
                                var message = await f.Result.Content.ReadAsStringAsync();
                                var stringMessage = JsonConvert.DeserializeObject<ResponseMessageModel>(message);

                                ModelState.AddModelError(string.Empty, stringMessage.Message);
                            }
                            else
                            {
                                ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("An error occurred when connect to api. Message = {0}", ex.Message);
                            Console.ForegroundColor = ConsoleColor.Gray;

                            ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("An error occurred when connect to api. Message = {0}", ex.Message);
                Console.ForegroundColor = ConsoleColor.Gray;

                ModelState.AddModelError(string.Empty, "خطای داخلی سیستم");
            }

            //if (!ModelState.IsValid)
            //    return View(registerLegalUserViewModel);
            if (!ModelState.IsValid)
            {
                TempData["ModelState"] = ModelState.GetErrors().Split(new char[] { '|' });
                return RedirectToAction(nameof(Register), nameof(AccountController).GetControllerName());

                //return RedirectToAction(nameof(Register), nameof(AccountController).GetControllerName(), new { message = "خطا: " + ModelState.GetErrors() });

                //ModelState.AddModelError(string.Empty, ModelState.GetErrors());
                //return View(registerLegalUserViewModel);
            }

            //_userRoleService.AddUserRole(new UserRole()
            //{
            //    UserId = userId,
            //    RoleId = 2
            //});
            //_userManager.AddToRoleAsync(user, "User");

            //return View("SuccessRegister", model: user);
            return RedirectToAction(nameof(ActivateAccount), nameof(AccountController).GetControllerName(), new
            {
                Mobile = registerLegalUserViewModel.PhoneNumber,
                message = "عملیات ثبت نام با موفقیت انجام و کد تایید عضویت به شماره موبایل ارسال شد و می توانید با وارد کردن کد تایید عضویت، حساب کاربری خود را فعال کنید"
            });
        }

        [HttpPost]
        public IActionResult RegisterRealUser(RegisterRealUserViewModel registerRealUserViewModel)
        {
            if (!ModelState.IsValid)
            {
                TempData["ModelState"] = ModelState.GetErrors().Split(new char[] { '|' });
                return RedirectToAction("Register", "Account");
                //return RedirectToAction("Register", "Account", new { message = "خطا: " + ModelState.GetErrors() });
                //ModelState.AddModelError(string.Empty, ModelState.GetErrors());
                //return View(registerRealUserViewModel);
            }

            if (_accountManagement.IsExsitPhoneNumber(registerRealUserViewModel.PhoneNumber))
            {
                //ModelState.AddModelError("Mobile", "موبایل تکراری است");
                //return View(registerRealUserViewModel);
                return RedirectToAction("Register", "Account", new
                {
                    message = "موبایل تکراری است"
                });
            }

            //if (_accountManagement.IsExsitPhone(registerRealUserViewModel.Phone))
            //{
            //    return RedirectToAction("Register", "Account", new
            //    {
            //        message = "تلفن تکراری است"
            //    });
            //}

            if (_accountManagement.IsExsitEmail(FixedText.FixedEmail(registerRealUserViewModel.Email)))
            {
                return RedirectToAction("Register", "Account", new
                {
                    message = "ایمیل تکراری است"
                });
            }

            /*
            if (_accountManagement.IsExsitNationalCode(registerRealUserViewModel.NationalCode))
            {
                return RedirectToAction("Register", "Account", new
                {
                    message = "کد ملی تکراری است"
                });
            }
            */

            TblUser user = _accountManagement.AddRealUser(registerRealUserViewModel);

            if (user == null)
                return RedirectToAction("Register", "Account", new
                {
                    message = "خطا: عملیات ثبت نام با شکست مواجه شد. لطفا مجددا تلاش بفرمایید"
                });

            //_userRoleService.AddUserRole(new UserRole()
            //{
            //    UserId = userId,
            //    RoleId = 2
            //});
            _userManager.AddToRoleAsync(user, "User");

            //return View("SuccessRegister", model: user);
            return RedirectToAction("ActivateAccount", "Account", new
            {
                Mobile = registerRealUserViewModel.PhoneNumber,
                message = "عملیات ثبت نام با موفقیت انجام و کد تایید عضویت به شماره موبایل ارسال شد و می توانید با وارد کردن کد تایید عضویت، حساب کاربری خود را فعال کنید"
            });
        }

        public IActionResult ActivateAccount()
        {
            return View();
        }

        public IActionResult ForgotPassword()
        {
            return View();
        }

        public IActionResult ResetPassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOut()
        //public IActionResult LogOut()
        {
            await _signInManager.SignOutAsync();
            //HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction(nameof(Login), nameof(AccountController).GetControllerName(), new
            {
                message = "با موفقیت از حساب کابری خود خارج شدید"
            });
        }

        #region Json Duplicate & Exist
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public async Task<JsonResult> DeleteGallery(int? GalleryID) => await db.SaveChangesAsync();
        public JsonResult DuplicateUserEmail(string email, string id)
        {
            if (id == null)
            {
                bool isExsit = _accountManagement.IsExsitEmail(FixedText.FixedEmail(email));
                if (isExsit == false)
                    return Json(true);
            }
            else
            {
                bool isExsit = _accountManagement.IsExsitEmailNotId(id, FixedText.FixedEmail(email));
                if (isExsit == false)
                    return Json(true);
            }

            return Json(false);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DuplicateUserPhoneNumber(string phoneNumber, string id)
        {
            if (id == null)
            {
                bool isExsit = _accountManagement.IsExsitPhoneNumber(FixedText.FixedEmail(phoneNumber));
                if (isExsit == false)
                    return Json(true);
            }
            else
            {
                bool isExsit = _accountManagement.IsExsitPhoneNumberNotId(id, FixedText.FixedEmail(phoneNumber));
                if (isExsit == false)
                    return Json(true);
            }

            return Json(false);
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public JsonResult DuplicateUserPhone(string phone, int? id)
        //{
        //    if (id == null)
        //    {
        //        bool isExsit = _accountManagement.IsExsitPhone(FixedText.FixedEmail(phone));
        //        if (isExsit == false)
        //            return Json(true);
        //    }
        //    else
        //    {
        //        bool isExsit = _accountManagement.IsExsitPhoneNotId(id, FixedText.FixedEmail(phone));
        //        if (isExsit == false)
        //            return Json(true);
        //    }

        //    return Json(false);
        //}

        /*
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DuplicateUserRegistrationNumber(string registrationNumber, string id)
        {
            if (id == null)
            {
                bool isExsit = _accountManagement.IsExsitRegistrationNumber(FixedText.FixedEmail(registrationNumber));
                if (isExsit == false)
                    return Json(true);
            }
            else
            {
                bool isExsit = _accountManagement.IsExsitRegistrationNumberNotId(id, FixedText.FixedEmail(registrationNumber));
                if (isExsit == false)
                    return Json(true);
            }

            return Json(false);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DuplicateUserNationalId(string nationalId, string id)
        {
            if (id == null)
            {
                bool isExsit = _accountManagement.IsExsitNationalId(FixedText.FixedEmail(nationalId));
                if (isExsit == false)
                    return Json(true);
            }
            else
            {
                bool isExsit = _accountManagement.IsExsitNationalIdNotId(id, FixedText.FixedEmail(nationalId));
                if (isExsit == false)
                    return Json(true);
            }

            return Json(false);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DuplicateUserEconomicCode(string economicCode, string id)
        {
            if (id == null)
            {
                bool isExsit = _accountManagement.IsExsitEconomicCode(FixedText.FixedEmail(economicCode));
                if (isExsit == false)
                    return Json(true);
            }
            else
            {
                bool isExsit = _accountManagement.IsExsitEconomicCodeNotId(id, FixedText.FixedEmail(economicCode));
                if (isExsit == false)
                    return Json(true);
            }

            return Json(false);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DuplicateUserNationalCode(string nationalCode, string id)
        {
            if (id == null)
            {
                bool isExsit = _accountManagement.IsExsitNationalCode(FixedText.FixedEmail(nationalCode));
                if (isExsit == false)
                    return Json(true);
            }
            else
            {
                bool isExsit = _accountManagement.IsExsitNationalCodeNotId(id, FixedText.FixedEmail(nationalCode));
                if (isExsit == false)
                    return Json(true);
            }

            return Json(false);
        }
        */

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ExistUserPhoneNumber(string phoneNumber)
        {
            bool isExsit = _accountManagement.IsExsitPhoneNumber(FixedText.FixedEmail(phoneNumber));
            if (isExsit == true)
                return Json(true);

            return Json(false);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ExistUserEmail(string email)
        {
            bool isExsit = _accountManagement.IsExsitEmail(FixedText.FixedEmail(email));
            if (isExsit == true)
                return Json(true);

            return Json(false);
        }
        #endregion
    }
}
