using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SCM.AccountManagement.Core;
using SCM.AccountManagement.Core.Implementations;
using SCM.AccountManagement.DataLayer;
using SCM.AccountManagement.Model;
using SCM.Infrastructure.Utility;

namespace SCM.UI.Razor
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region Razor View
            services.AddControllersWithViews()
                // jahate inke bade az run shodan ham, taghirate view ha ra be lahze bebinim
                .AddRazorRuntimeCompilation();
            #endregion

            #region Razor Page
            services.AddRazorPages();
            #endregion

            #region COPY FROM STARTUP OF SCM.AccountManagement.Microservice
            #region Database Context
            services.AddDbContext<AccountManagementContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("ScmConnection"));
            });
            #endregion

            #region IoC (In Version of Control)
            services.AddTransient<IAccountManagement, AccountManagementSqlServer>();
            #endregion

            #region Identity
            //services.AddIdentity<IdentityUser, IdentityRole>()
            services.AddIdentity<TblUser, IdentityRole>()
                .AddEntityFrameworkStores<AccountManagementContext>()
                // jahate shakhsi sazie error haye identity
                .AddErrorDescriber<MultiLanguageErrorMessage>()
                // jahate eijad token e marbut be etebar sanjie link haei mesle taeide hesabe karbari va ... ast
                .AddDefaultTokenProviders();

            // jahate shakhsi sazie validation haye marbut be identity
            services.Configure<IdentityOptions>(options => {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 5;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                //options.Password.RequiredUniqueChars = 0;
                options.User.RequireUniqueEmail = true;
                //options.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-";
                // jahate taeine modat zamane ghofl shodane hesabe karbari bad az 5 bar eshtebah zadane ramze obur
                //options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(15);
            });
            #endregion

            #region Authentication
            //services.AddAuthentication(options =>
            //{
            //    options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            //    options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            //    options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            //}).AddCookie(options =>
            //{
            //    options.LoginPath = "/Account/Login";
            //    options.LogoutPath = "/Account/Logout";
            //    options.ExpireTimeSpan = TimeSpan.FromMinutes(43200); // 1 month
            //});
            #endregion
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                #region Razor View
                app.UseExceptionHandler("/Home/Error");
                #endregion

                #region Razor Page
                //app.UseExceptionHandler("/Error");
                //// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //app.UseHsts();
                #endregion
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            #region Authentication
            app.UseAuthentication();
            #endregion

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                #region Razor View
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                #endregion

                #region Razor Page
                endpoints.MapRazorPages();
                #endregion
            });
        }
    }
}
