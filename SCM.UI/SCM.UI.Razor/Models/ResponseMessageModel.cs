﻿namespace SCM.UI.Razor.Models
{
    public class ResponseMessageModel
    {
        public string Message { get; set; }
        public bool Status { get; set; }
    }
}
