﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SCM.AccountManagement.Core.Models;
using SCM.AccountManagement.DataLayer;
using SCM.AccountManagement.Model;
using SCM.AccountManagement.Model.ViewModels.Account;
using SCM.Infrastructure.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SCM.AccountManagement.Core.Implementations
{
    public class AccountManagementSqlServer : IAccountManagement
    {
        private IHostingEnvironment _hostingEnvironment;
        private AccountManagementContext _context;
        private UserManager<TblUser> _userManager;
        private SignInManager<TblUser> _signInManager;

        public AccountManagementSqlServer(IHostingEnvironment hostingEnvironment, AccountManagementContext context, UserManager<TblUser> userManager, SignInManager<TblUser> signInManager)
        {
            _hostingEnvironment = hostingEnvironment;
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public bool IsExsitEmail(string email)
        {
            return _context.Users.Any(u => u.Email == email);
        }

        public bool IsExsitEmailNotId(string id, string email)
        {
            return _context.Users.Any(u => u.Id != id && u.Email == email);
        }

        public bool IsExsitPhoneNumber(string phoneNumber)
        {
            return _context.Users.Any(u => u.PhoneNumber == phoneNumber);
        }

        public bool IsExsitPhoneNumberNotId(string id, string phoneNumber)
        {
            return _context.Users.Any(u => u.Id != id && u.PhoneNumber == phoneNumber);
        }

        /*
        public bool IsExsitRegistrationNumber(string registrationNumber)
        {
            return _context.Users.Any(u => u.RegistrationNumber == registrationNumber);
        }

        public bool IsExsitRegistrationNumberNotId(string id, string registrationNumber)
        {
            return _context.Users.Any(u => u.Id != id && u.RegistrationNumber == registrationNumber);
        }

        public bool IsExsitNationalId(string nationalId)
        {
            return _context.Users.Any(u => u.NationalId == nationalId);
        }

        public bool IsExsitNationalIdNotId(string id, string nationalId)
        {
            return _context.Users.Any(u => u.Id != id && u.NationalId == nationalId);
        }

        public bool IsExsitEconomicCode(string economicCode)
        {
            return _context.Users.Any(u => u.EconomicCode == economicCode);
        }

        public bool IsExsitEconomicCodeNotId(string id, string economicCode)
        {
            return _context.Users.Any(u => u.Id != id && u.EconomicCode == economicCode);
        }

        public bool IsExsitNationalCode(string nationalCode)
        {
            return _context.Users.Any(u => u.NationalCode == nationalCode);
        }

        public bool IsExsitNationalCodeNotId(string id, string nationalCode)
        {
            return _context.Users.Any(u => u.Id != id && u.NationalCode == nationalCode);
        }
        */

        public async Task<bool> IsExsitPhoneNumberPassword(string phoneNumber, string password)
        {
            //string _password = PasswordHelper.EncodePasswordMd5(password);
            //return _context.Users.Any(u => u.PhoneNumber == phoneNumber && u.PasswordHash == _password);

            TblUser tblUser = new TblUser();
            tblUser = _context.Users.Where(u => u.PhoneNumber == phoneNumber).SingleOrDefault();
            if (tblUser == null)
                return false;

            //var result = await _signInManager.CheckPasswordSignInAsync(tblUser, password, true);
            var result = await _signInManager.PasswordSignInAsync(tblUser.UserName, password, true, true);
            //var result = await _signInManager.PasswordSignInAsync(tblUser.UserName, password, true, true);
            //var result = await _signInManager.SignInAsync(tblUser, true);
            if (result.Succeeded)
                return true;

            return false;
        }

        public async Task<bool> AddLegalUser(LegalUserModel registerLegalUserViewModel)
        {
            //string imageName = null;
            //if (registerLegalUserViewModel.ImageFile != null)
            //{
            //    //string[] imageType = registerLegalUserViewModel.ImageFile.ContentType.Split(new char[] { '/' });
            //    //imageName = GeneratorName.GenrateUniqeCode() + "." + imageType[1];
            //    //string Path = _hostingEnvironment.WebRootPath + @"\img\user\" + imageName;
            //    //using (FileStream fs = System.IO.File.Create(Path))
            //    //{
            //    //    await registerLegalUserViewModel.ImageFile.CopyToAsync(fs);
            //    //    await fs.FlushAsync();
            //    //}

            //    imageName = GeneratorName.GenrateUniqeCode() + Path.GetExtension(registerLegalUserViewModel.ImageFile.FileName);
            //    string imagePath = Path.Combine(_hostingEnvironment.WebRootPath, @"img\user\", imageName);
            //    using (var stream = new FileStream(imagePath, FileMode.Create))
            //    {
            //        registerLegalUserViewModel.ImageFile.CopyTo(stream);
            //    }
            //}

            TblUser user = new TblUser()
            {
                UserName = FixedText.FixedEmail(registerLegalUserViewModel.Email),
                UserType = "حقوقی",
                FirstName = registerLegalUserViewModel.FirstName,
                LastName = registerLegalUserViewModel.LastName,
                PhoneNumber = registerLegalUserViewModel.PhoneNumber,
                Email = FixedText.FixedEmail(registerLegalUserViewModel.Email),
                //PasswordHash = PasswordHelper.EncodePasswordMd5(registerLegalUserViewModel.Password),
                //ImageName = imageName,
                /*
                RegistrationNumber = registerLegalUserViewModel.RegistrationNumber,
                RegistrationPlace = registerLegalUserViewModel.RegistrationPlace,
                RegistrationDate = DateConvertor.StringToDatetime(DigitConvertor.PersianToEnglish(registerLegalUserViewModel.RegistrationDate)),
                NationalId = registerLegalUserViewModel.NationalId,
                EconomicCode = registerLegalUserViewModel.EconomicCode,
                Address = registerLegalUserViewModel.Address,
                */
                ActiveCode = GeneratorName.GenrateUniqeCode(),
                CreateDate = DateTime.Now,
                IsActive = false,
                IsDelete = false
            };

            #region Send Activation SMS
            //if (!SendSms.SendVerification(new VerificationParametr()
            //{
            //    PhoneNumber = user.PhoneNumber,
            //    ActiveCode = user.ActiveCode
            //}))
            //    return null;
            #endregion

            #region Send Activation Email
            //if (!SendEmail.Send(new EmailParametr()
            //{
            //    Subject = "فعال سازی حساب کاربری",
            //    To = user.Email,
            //    //Body = _viewRender.RenderToStringAsync("_ActiveEmail", user)
            //    Body = "<div style='direction:rtl; text-align:right'>سلام کاربر محترم</div>"
            //}))
            //    return null;
            #endregion

            #region Add User and Role
            //_context.Users.Add(user);
            //_context.SaveChanges();

            var result = await _userManager.CreateAsync(user, registerLegalUserViewModel.Password);
            if (!result.Succeeded)
                //result2.Errors
                return false;

            var result2 = await _userManager.AddToRoleAsync(user, "User");
            //var result2 = await _userManager.AddToRolesAsync(user, "User");
            if (!result2.Succeeded)
                //result2.Errors
                return false;
            #endregion

            return true;
        }

        public TblUser AddRealUser(RegisterRealUserViewModel registerRealUserViewModel)
        {
            /*
            string imageName = null;
            if (registerRealUserViewModel.ImageFile != null)
            {
                //string[] imageType = registerLegalUserViewModel.ImageFile.ContentType.Split(new char[] { '/' });
                //imageName = GeneratorName.GenrateUniqeCode() + "." + imageType[1];
                //string Path = _hostingEnvironment.WebRootPath + @"\img\user\" + imageName;
                //using (FileStream fs = System.IO.File.Create(Path))
                //{
                //    await registerLegalUserViewModel.ImageFile.CopyToAsync(fs);
                //    await fs.FlushAsync();
                //}
                imageName = GeneratorName.GenrateUniqeCode() + Path.GetExtension(registerRealUserViewModel.ImageFile.FileName);
                string imagePath = Path.Combine(_hostingEnvironment.WebRootPath, @"img\user\", imageName);
                using (var stream = new FileStream(imagePath, FileMode.Create))
                {
                    registerRealUserViewModel.ImageFile.CopyTo(stream);
                }
            }
            */

            TblUser user = new TblUser()
            {
                UserType = "حقیقی",
                FirstName = registerRealUserViewModel.FirstName,
                LastName = registerRealUserViewModel.LastName,
                PhoneNumber = registerRealUserViewModel.PhoneNumber,
                Email = FixedText.FixedEmail(registerRealUserViewModel.Email),
                PasswordHash = PasswordHelper.EncodePasswordMd5(registerRealUserViewModel.Password),
                /*
                ImageName = imageName,
                DateOfBirth = DateConvertor.StringToDatetime(DigitConvertor.PersianToEnglish(registerRealUserViewModel.DateOfBirth)),
                NationalCode = registerRealUserViewModel.NationalCode,
                */
                ActiveCode = GeneratorName.GenrateUniqeCode(),
                CreateDate = DateTime.Now,
                IsActive = false,
                IsDelete = false
            };

            #region Send Activation SMS
            //if (!SendSms.SendVerification(new VerificationParametr()
            //{
            //    PhoneNumber = user.PhoneNumber,
            //    ActiveCode = user.ActiveCode
            //}))
            //    return null;
            #endregion

            #region Send Activation Email
            //if (!SendEmail.Send(new EmailParametr()
            //{
            //    Subject = "فعال سازی حساب کاربری",
            //    To = user.Email,
            //    //Body = _viewRender.RenderToStringAsync("_ActiveEmail", user)
            //    Body = "<div style='direction:rtl; text-align:right'>سلام کاربر محترم</div>"
            //}))
            //    return null;
            #endregion

            _context.Users.Add(user);
            _context.SaveChanges();
            //var res = _userManager.CreateAsync(user, user.PasswordHash);
            //_userManager.AddToRoleAsync(user, "User");
            return user;
        }

        public TblUser GetUserByPhoneNumberPassword(string phoneNumber, string password)
        {
            string _password = PasswordHelper.EncodePasswordMd5(password);
            //return _context.Users.SingleOrDefault(u => u.PhoneNumber == phoneNumber && u.Password == _password);
            return _context.Users.Where(u => u.PhoneNumber == phoneNumber && u.PasswordHash == _password)
                //.Include(a => a.UserRoles)
                //    .ThenInclude(a => a.Role)
                .SingleOrDefault();
        }

        public TblUser GetUserByPhoneNumber(string phoneNumber)
        {
            return _context.Users.Where(u => u.PhoneNumber == phoneNumber)
                //.Include(a => a.UserRoles)
                //    .ThenInclude(a => a.Role)
                .SingleOrDefault();
        }

        public List<TblUser> GetAllUser()
        {
            return _context.Users.ToList();
        }

        #region TUTORIAL
        //public async Task<List<TblUser>> GetAsyncHamid()
        //{
        //    return await _context.Users.ToListAsync();
        //}

        //public List<TblUser> GetHamid()
        //{
        //    return _context.Users.ToList();
        //}

        //public string AddUser(TblUser user)
        //{
        //    //_context.Users.Add(user);
        //    //_context.SaveChanges();
        //    //return user.Id;
        //    _userManager.CreateAsync(user, user.PasswordHash);
        //    return user.Id;
        //}

        //public User GetUserByEmail(string email)
        //{
        //    return _context.Users.SingleOrDefault(u => u.Email == email);
        //}

        //public User GetUserByActiveCode(string activeCode)
        //{
        //    return _context.Users.SingleOrDefault(u => u.ActiveCode == activeCode);
        //}

        //public int GetUserIdByEmail(string email)
        //{
        //    return _context.Users.Single(u => u.Email == email).Id;
        //}

        //public void UpdateUser(User user)
        //{
        //    _context.Update(user);
        //    _context.SaveChanges();
        //}

        //public bool ActiveAccount(string activeCode)
        //{
        //    var user = _context.Users.SingleOrDefault(u => u.ActiveCode == activeCode);
        //    if (user == null || user.IsActive)
        //        return false;

        //    user.IsActive = true;
        //    user.ActiveCode = GeneratorName.GenrateUniqeCode();

        //    _context.Users.Update(user);
        //    _context.SaveChanges();
        //    return true;
        //}
        #endregion
    }
}
