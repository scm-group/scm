﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCM.AccountManagement.Core.Models
{
    public class LegalUserModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        /*
        public IFormFile ImageFile { get; set; }
        public string RegistrationNumber { get; set; }
        public string RegistrationPlace { get; set; }
        public string RegistrationDate { get; set; }
        public string NationalId { get; set; }
        public string EconomicCode { get; set; }
        public string Address { get; set; }
        */
    }
}
