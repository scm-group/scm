﻿using SCM.AccountManagement.Core.Models;
using SCM.AccountManagement.Model;
using SCM.AccountManagement.Model.ViewModels.Account;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SCM.AccountManagement.Core
{
    public interface IAccountManagement
    {
        bool IsExsitEmail(string email);
        bool IsExsitEmailNotId(string id, string email);
        bool IsExsitPhoneNumber(string phoneNumber);
        bool IsExsitPhoneNumberNotId(string id, string phoneNumber);
        /*
        bool IsExsitRegistrationNumber(string registrationNumber);
        bool IsExsitRegistrationNumberNotId(string id, string registrationNumber);
        bool IsExsitNationalId(string nationalId);
        bool IsExsitNationalIdNotId(string id, string nationalId);
        bool IsExsitEconomicCode(string economicCode);
        bool IsExsitEconomicCodeNotId(string id, string economicCode);
        bool IsExsitNationalCode(string nationalCode);
        bool IsExsitNationalCodeNotId(string id, string nationalCode);
        */

        Task<bool> IsExsitPhoneNumberPassword(string phoneNumber, string password);

        Task<bool> AddLegalUser(LegalUserModel registerLegalUserViewModel);
        TblUser AddRealUser(RegisterRealUserViewModel registerRealUserViewModel);
        TblUser GetUserByPhoneNumberPassword(string phoneNumber, string password);

        TblUser GetUserByPhoneNumber(string phoneNumber);

        List<TblUser> GetAllUser();
    }
}
