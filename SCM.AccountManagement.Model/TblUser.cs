﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace SCM.AccountManagement.Model
{
    public class TblUser : IdentityUser
    {
        //[Key]
        ////[Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        //public int Id { get; set; }

        //[Display(Name = "نوع کاربر")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        //[RegularExpression(RegularExpressionFormat.UserTypeRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        public string UserType { get; set; }

        //[Display(Name = "نام")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        //[MaxLength(100, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        //[RegularExpression(RegularExpressionFormat.TitleRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        public string FirstName { get; set; }

        //[Display(Name = "نام خانوادگی")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        //[MaxLength(100, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        //[RegularExpression(RegularExpressionFormat.TitleRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        public string LastName { get; set; }

        //[Display(Name = "موبایل")]
        //[DataType(DataType.PhoneNumber)]
        //[Phone(ErrorMessage = ErrorMessage.FormatMsg)]
        //[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        //[StringLength(13, MinimumLength = 7, ErrorMessage = ErrorMessage.StringLengthMsg)]
        //[RegularExpression(RegularExpressionFormat.MobileRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        //[Remote("DuplicateUserMobile", "Account", ErrorMessage = ErrorMessage.DuplicateMsg, HttpMethod = "POST")]
        //public string Mobile { get; set; }

        //[Display(Name = "تلفن")]
        //[DataType(DataType.PhoneNumber)]
        //[Phone(ErrorMessage = ErrorMessage.FormatMsg)]
        //[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        ////[StringLength(13, MinimumLength = 7, ErrorMessage = ErrorMessage.StringLengthMsg)]
        //[RegularExpression(RegularExpressionFormat.PhoneRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        //[Remote("DuplicateUserPhone", "Account", ErrorMessage = ErrorMessage.DuplicateMsg, HttpMethod = "POST")]
        //public string Phone { get; set; }

        //[Display(Name = "ایمیل")]
        //[DataType(DataType.EmailAddress)]
        //[EmailAddress(ErrorMessage = ErrorMessage.FormatMsg)]
        //[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        //[MaxLength(100, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        //[RegularExpression(RegularExpressionFormat.EmailRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        //[Remote("DuplicateUserEmail", "Account", ErrorMessage = ErrorMessage.DuplicateMsg, HttpMethod = "POST")]
        //public string Email { get; set; }

        //[Display(Name = "کلمه عبور")]
        //[DataType(DataType.Password)]
        //[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        //[StringLength(100, MinimumLength = 6, ErrorMessage = ErrorMessage.StringLengthMsg)]
        //[RegularExpression(RegularExpressionFormat.PasswordRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        //public string Password { get; set; }

        /*
        //[Display(Name = "نام عکس")]
        ////[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        public string ImageName { get; set; }
        */

        //[Display(Name = "کد فعال سازی")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        public string ActiveCode { get; set; }

        //[Display(Name = "تاریخ عضویت")]
        //[DataType(DataType.Date)]
        //[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        public DateTime CreateDate { get; set; }

        //[Display(Name = "تاریخ بروز رسانی")]
        //[DataType(DataType.Date)]
        public Nullable<System.DateTime> DateUpdate { get; set; }

        //[Display(Name = "وضعیت فعال سازی")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        public bool IsActive { get; set; }

        //[Display(Name = "وضعیت حذف")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        public bool IsDelete { get; set; }




        /*
        //[Display(Name = "شماره ثبت")]
        ////[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        ////[MaxLength(100, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        ////[RegularExpression(RegularExpressionFormat.PhoneRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        ////[Remote("DuplicateUserRegistrationNumber", "Account", ErrorMessage = ErrorMessage.DuplicateMsg, HttpMethod = "POST")]
        public string RegistrationNumber { get; set; }

        //[Display(Name = "محل ثبت")]
        ////[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        ////[MaxLength(100, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        ////[RegularExpression(RegularExpressionFormat.PhoneRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        public string RegistrationPlace { get; set; }

        //[Display(Name = "تاریخ ثبت")]
        //[DataType(DataType.Date)]
        ////[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        public Nullable<System.DateTime> RegistrationDate { get; set; }

        //[Display(Name = "شناسه ملی")]
        ////[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        ////[MaxLength(100, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        ////[RegularExpression(RegularExpressionFormat.PhoneRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        ////[Remote("DuplicateUserNationalId", "Account", ErrorMessage = ErrorMessage.DuplicateMsg, HttpMethod = "POST")]
        public string NationalId { get; set; }

        //[Display(Name = "کد اقتصادی")]
        ////[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        ////[MaxLength(100, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        ////[RegularExpression(RegularExpressionFormat.PhoneRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        ////[Remote("DuplicateUserEconomicCode", "Account", ErrorMessage = ErrorMessage.DuplicateMsg, HttpMethod = "POST")]
        public string EconomicCode { get; set; }

        //[Display(Name = "آدرس")]
        //[DataType(DataType.MultilineText)]
        //[MaxLength(500, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        //[RegularExpression(RegularExpressionFormat.TextRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        public string Address { get; set; }





        //[Display(Name = "تاریخ تولد")]
        //[DataType(DataType.Date)]
        ////[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        public Nullable<System.DateTime> DateOfBirth { get; set; }

        //[Display(Name = "کد ملی")]
        ////[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        ////[MaxLength(100, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        ////[RegularExpression(RegularExpressionFormat.PhoneRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        ////[Remote("DuplicateUserNationalCode", "Account", ErrorMessage = ErrorMessage.DuplicateMsg, HttpMethod = "POST")]
        public string NationalCode { get; set; }
        */
    }
}
