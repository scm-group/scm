﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SCM.Infrastructure.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SCM.AccountManagement.Model.ViewModels.Account
{
    public class RegisterLegalUserViewModel
    {
        //[Display(Name = "نوع کاربر")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        //[RegularExpression(RegularExpressionFormat.UserTypeRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        //public string UserType { get; set; }

        [Display(Name = "نام")]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        [MaxLength(100, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        [RegularExpression(RegularExpressionFormat.TitleRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        public string FirstName { get; set; }

        [Display(Name = "نام خانوادگی")]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        [MaxLength(100, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        [RegularExpression(RegularExpressionFormat.TitleRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        public string LastName { get; set; }

        [Display(Name = "موبایل")]
        [DataType(DataType.PhoneNumber)]
        [Phone(ErrorMessage = ErrorMessage.FormatMsg)]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        [StringLength(13, MinimumLength = 7, ErrorMessage = ErrorMessage.StringLengthMsg)]
        [RegularExpression(RegularExpressionFormat.PhoneNumberRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        [Remote("DuplicateUserPhoneNumber", "Account", ErrorMessage = ErrorMessage.DuplicateMsg, HttpMethod = "POST", AdditionalFields = "__RequestVerificationToken")]
        public string PhoneNumber { get; set; }

        //[Display(Name = "تلفن")]
        //[DataType(DataType.PhoneNumber)]
        //[Phone(ErrorMessage = ErrorMessage.FormatMsg)]
        //[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        ////[StringLength(13, MinimumLength = 7, ErrorMessage = ErrorMessage.StringLengthMsg)]
        //[RegularExpression(RegularExpressionFormat.PhoneRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        //[Remote("DuplicateUserPhone", "Account", ErrorMessage = ErrorMessage.DuplicateMsg, HttpMethod = "POST", AdditionalFields = "__RequestVerificationToken")]
        //public string Phone { get; set; }

        [Display(Name = "ایمیل")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = ErrorMessage.FormatMsg)]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        [MaxLength(100, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        [RegularExpression(RegularExpressionFormat.EmailRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        [Remote("DuplicateUserEmail", "Account", ErrorMessage = ErrorMessage.DuplicateMsg, HttpMethod = "POST", AdditionalFields = "__RequestVerificationToken")]
        public string Email { get; set; }

        [Display(Name = "کلمه عبور")]
        [DataType(DataType.Password)]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        [StringLength(100, MinimumLength = 5, ErrorMessage = ErrorMessage.StringLengthMsg)]
        [RegularExpression(RegularExpressionFormat.PasswordRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        public string Password { get; set; }

        [Display(Name = "تکرار رمز عبور")]
        [DataType(DataType.Password)]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        [StringLength(100, MinimumLength = 5, ErrorMessage = ErrorMessage.StringLengthMsg)]
        [RegularExpression(RegularExpressionFormat.PasswordRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        [Compare("Password", ErrorMessage = ErrorMessage.CompareLengthMsg)]
        public string RepeatPassword { get; set; }

        /*
        [Display(Name = "فایل عکس")]
        //[NotMapped]
        //[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        [CheckFile(false, 512, "image/jpg,image/jpeg")]
        public IFormFile ImageFile { get; set; }
        */

        #region Legal User Exclusive Field
        /*
        [Display(Name = "شماره ثبت")]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        //[MaxLength(100, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        //[RegularExpression(RegularExpressionFormat.PhoneRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        [Remote("DuplicateUserRegistrationNumber", "Account", ErrorMessage = ErrorMessage.DuplicateMsg, HttpMethod = "POST", AdditionalFields = "__RequestVerificationToken")]
        public string RegistrationNumber { get; set; }

        [Display(Name = "محل ثبت")]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        //[MaxLength(100, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        //[RegularExpression(RegularExpressionFormat.PhoneRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        public string RegistrationPlace { get; set; }

        [Display(Name = "تاریخ ثبت")]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        [RegularExpression(RegularExpressionFormat.DatePersianRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        public string RegistrationDate { get; set; }

        [Display(Name = "شناسه ملی")]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        //[MaxLength(100, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        //[RegularExpression(RegularExpressionFormat.PhoneRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        [Remote("DuplicateUserNationalId", "Account", ErrorMessage = ErrorMessage.DuplicateMsg, HttpMethod = "POST", AdditionalFields = "__RequestVerificationToken")]
        public string NationalId { get; set; }

        [Display(Name = "کد اقتصادی")]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        //[MaxLength(100, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        //[RegularExpression(RegularExpressionFormat.PhoneRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        [Remote("DuplicateUserEconomicCode", "Account", ErrorMessage = ErrorMessage.DuplicateMsg, HttpMethod = "POST", AdditionalFields = "__RequestVerificationToken")]
        public string EconomicCode { get; set; }

        [Display(Name = "آدرس")]
        [DataType(DataType.MultilineText)]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        [MaxLength(500, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        [RegularExpression(RegularExpressionFormat.TextRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        public string Address { get; set; }
        */
        #endregion
    }

    public class RegisterRealUserViewModel
    {
        //[Display(Name = "نوع کاربر")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        //[RegularExpression(RegularExpressionFormat.UserTypeRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        //public string UserType { get; set; }

        [Display(Name = "نام")]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        [MaxLength(100, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        [RegularExpression(RegularExpressionFormat.TitleRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        public string FirstName { get; set; }

        [Display(Name = "نام خانوادگی")]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        [MaxLength(100, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        [RegularExpression(RegularExpressionFormat.TitleRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        public string LastName { get; set; }

        [Display(Name = "موبایل")]
        [DataType(DataType.PhoneNumber)]
        [Phone(ErrorMessage = ErrorMessage.FormatMsg)]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        [StringLength(13, MinimumLength = 7, ErrorMessage = ErrorMessage.StringLengthMsg)]
        [RegularExpression(RegularExpressionFormat.PhoneNumberRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        [Remote("DuplicateUserPhoneNumber", "Account", ErrorMessage = ErrorMessage.DuplicateMsg, HttpMethod = "POST", AdditionalFields = "__RequestVerificationToken")]
        public string PhoneNumber { get; set; }

        //[Display(Name = "تلفن")]
        //[DataType(DataType.PhoneNumber)]
        //[Phone(ErrorMessage = ErrorMessage.FormatMsg)]
        //[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        ////[StringLength(13, MinimumLength = 7, ErrorMessage = ErrorMessage.StringLengthMsg)]
        //[RegularExpression(RegularExpressionFormat.PhoneRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        //[Remote("DuplicateUserPhone", "Account", ErrorMessage = ErrorMessage.DuplicateMsg, HttpMethod = "POST", AdditionalFields = "__RequestVerificationToken")]
        //public string Phone { get; set; }

        [Display(Name = "ایمیل")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = ErrorMessage.FormatMsg)]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        [MaxLength(100, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        [RegularExpression(RegularExpressionFormat.EmailRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        [Remote("DuplicateUserEmail", "Account", ErrorMessage = ErrorMessage.DuplicateMsg, HttpMethod = "POST", AdditionalFields = "__RequestVerificationToken")]
        public string Email { get; set; }

        [Display(Name = "کلمه عبور")]
        [DataType(DataType.Password)]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        [StringLength(100, MinimumLength = 5, ErrorMessage = ErrorMessage.StringLengthMsg)]
        [RegularExpression(RegularExpressionFormat.PasswordRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        public string Password { get; set; }

        [Display(Name = "تکرار رمز عبور")]
        [DataType(DataType.Password)]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        [StringLength(100, MinimumLength = 5, ErrorMessage = ErrorMessage.StringLengthMsg)]
        [RegularExpression(RegularExpressionFormat.PasswordRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        [Compare("Password", ErrorMessage = ErrorMessage.CompareLengthMsg)]
        public string RepeatPassword { get; set; }

        /*
        [Display(Name = "فایل عکس")]
        //[NotMapped]
        //[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        [CheckFile(false, 512, "image/jpg,image/jpeg")]
        public IFormFile ImageFile { get; set; }
        */

        #region Legal User Exclusive Field
        /*
        [Display(Name = "تاریخ تولد")]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        [RegularExpression(RegularExpressionFormat.DatePersianRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        public string DateOfBirth { get; set; }

        [Display(Name = "کد ملی")]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        //[MaxLength(100, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        //[RegularExpression(RegularExpressionFormat.PhoneRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        [Remote("DuplicateUserNationalCode", "Account", ErrorMessage = ErrorMessage.DuplicateMsg, HttpMethod = "POST", AdditionalFields = "__RequestVerificationToken")]
        public string NationalCode { get; set; }
        */
        #endregion
    }

    public class LoginViewModel
    {
        //[Display(Name = "ایمیل")]
        //[DataType(DataType.EmailAddress)]
        //[EmailAddress(ErrorMessage = ErrorMessage.FormatMsg)]
        //[Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        //[MaxLength(100, ErrorMessage = ErrorMessage.MaxLengthMsg)]
        //[RegularExpression(RegularExpressionFormat.EmailRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        //[Remote("ExistUserEmail", "Account", ErrorMessage = ErrorMessage.ExistMsg, HttpMethod = "POST", AdditionalFields = "__RequestVerificationToken")]
        //public string Email { get; set; }

        [Display(Name = "موبایل")]
        [DataType(DataType.PhoneNumber)]
        [Phone(ErrorMessage = ErrorMessage.FormatMsg)]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        [StringLength(13, MinimumLength = 7, ErrorMessage = ErrorMessage.StringLengthMsg)]
        [RegularExpression(RegularExpressionFormat.PhoneNumberRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        [Remote("ExistUserPhoneNumber", "Account", ErrorMessage = ErrorMessage.ExistMsg, HttpMethod = "POST", AdditionalFields = "__RequestVerificationToken")]
        public string PhoneNumber { get; set; }

        [Display(Name = "کلمه عبور")]
        [DataType(DataType.Password)]
        [Required(AllowEmptyStrings = false, ErrorMessage = ErrorMessage.RequiredMsg)]
        [StringLength(100, MinimumLength = 5, ErrorMessage = ErrorMessage.StringLengthMsg)]
        [RegularExpression(RegularExpressionFormat.PasswordRegularExpression, ErrorMessage = ErrorMessage.RegularExpressionMsg)]
        public string Password { get; set; }

        [Display(Name = "مرا به خاطر بسپار")]
        public bool RememberMe { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        //[Display(Name = "ایمیل")]
        //[Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        //[MaxLength(200, ErrorMessage = "{0} نمی تواند بیشتر از {1} کاراکتر باشد.")]
        //[EmailAddress(ErrorMessage = "ایمیل وارد شده معتبر نیست")]
        //public string Email { get; set; }
    }

    public class ResetPasswordViewModel
    {
        //public string ActiveCode { get; set; }

        //[Display(Name = "کلمه عبور")]
        //[Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        //[MaxLength(200, ErrorMessage = "{0} نمی تواند بیشتر از {1} کاراکتر باشد.")]
        //public string Password { get; set; }

        //[Display(Name = "تکرار کلمه عبور")]
        //[Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        //[MaxLength(200, ErrorMessage = "{0} نمی تواند بیشتر از {1} کاراکتر باشد.")]
        //[Compare("Password", ErrorMessage = "کلمه عبور مغایرت دارد")]
        //public string RePassword { get; set; }
    }
}
