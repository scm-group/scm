﻿using FluentAssertions;
using SCM.FileManagement.Core.Exceptions;
using SCM.FileManagement.Core.Implementation;
using System;
using System.IO;
using Xunit;

namespace SCM.FileManagement.Core.Test
{
    public class LocalStorageFileHelperTest
    {
        [Fact]
        [CheckedException.Core.ThrowsException(typeof(LocalStorageException))]
        [CheckedException.Core.ThrowsException(typeof(System.IO.FileNotFoundException))]
        public void SaveBillingFileAsyncTest()
        {
            var lsfh = new LocalStorageFileHelper(string.Empty);

            Func<string> act = () => lsfh.SaveBillingFileAsync(null, string.Empty).Result;
            act.Should().Throw<LocalStorageException>().WithMessage("Invalid directory: \"\"");

            lsfh = new LocalStorageFileHelper("Files");
            act = () => lsfh.SaveBillingFileAsync(null, string.Empty).Result;
            act.Should().Throw<LocalStorageException>().WithMessage("Input file cannot be null or empty.");

            var file = new byte[] { 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1};

            act = () => lsfh.SaveBillingFileAsync(file, string.Empty).Result;
            act.Should().Throw<LocalStorageException>().WithMessage("Invalid file extension: \"\"");

            var fileName = lsfh.SaveBillingFileAsync(file, "....png").Result;
            var now = DateTime.Now;
            fileName.Should().StartWith($"Bill_{now.Year:0000}{now.Month:00}{now.Day}_{now.Hour:00}{now.Minute:00}_")
                .And
                .EndWith(".png");

            lsfh = new LocalStorageFileHelper(string.Empty);
            Func<byte[]> act2 = () => lsfh.GetBillingFileAsync(string.Empty).Result;
            act2.Should().Throw<LocalStorageException>().WithMessage("Invalid directory: \"\"");

            lsfh = new LocalStorageFileHelper("InvalidFolder");
            act2 = () => lsfh.GetBillingFileAsync(string.Empty).Result;
            act2.Should().Throw<LocalStorageException>().WithMessage("File name cannot be null or empty.");

            act2 = () => lsfh.GetBillingFileAsync(fileName).Result;
            act2.Should().Throw<FileNotFoundException>().WithMessage($"File with name \"{fileName}\" not found.");

            lsfh = new LocalStorageFileHelper("Files");
            act2 = () => lsfh.GetBillingFileAsync("InvalidFileName.sst").Result;
            act2.Should().Throw<FileNotFoundException>().WithMessage($"File with name \"InvalidFileName.sst\" not found.");

            var readFile = lsfh.GetBillingFileAsync(fileName).Result;
            readFile.Should()
                .NotBeNull()
                .And
                .HaveCount(11)
                .And
                .BeEquivalentTo(file);
        }

        [Fact]
        [CheckedException.Core.ThrowsException(typeof(LocalStorageException))]
        [CheckedException.Core.ThrowsException(typeof(System.IO.FileNotFoundException))]
        public void SaveRegistrationFileAsyncTest()
        {
            var lsfh = new LocalStorageFileHelper(string.Empty);

            Func<string> act = () => lsfh.SaveRegistrationFileAsync(null, string.Empty).Result;
            act.Should().Throw<LocalStorageException>().WithMessage("Invalid directory: \"\"");

            lsfh = new LocalStorageFileHelper("Files");
            act = () => lsfh.SaveRegistrationFileAsync(null, string.Empty).Result;
            act.Should().Throw<LocalStorageException>().WithMessage("Input file cannot be null or empty.");

            var file = new byte[] { 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1 };

            act = () => lsfh.SaveRegistrationFileAsync(file, string.Empty).Result;
            act.Should().Throw<LocalStorageException>().WithMessage("Invalid file extension: \"\"");

            var fileName = lsfh.SaveRegistrationFileAsync(file, "....png").Result;
            var now = DateTime.Now;
            fileName.Should().StartWith($"Registration_{now.Year:0000}{now.Month:00}{now.Day}_{now.Hour:00}{now.Minute:00}_")
                .And
                .EndWith(".png");

            lsfh = new LocalStorageFileHelper(string.Empty);
            Func<byte[]> act2 = () => lsfh.GetRegistrationFileAsync(string.Empty).Result;
            act2.Should().Throw<LocalStorageException>().WithMessage("Invalid directory: \"\"");

            lsfh = new LocalStorageFileHelper("InvalidFolder");
            act2 = () => lsfh.GetRegistrationFileAsync(string.Empty).Result;
            act2.Should().Throw<LocalStorageException>().WithMessage("File name cannot be null or empty.");

            act2 = () => lsfh.GetRegistrationFileAsync(fileName).Result;
            act2.Should().Throw<FileNotFoundException>().WithMessage($"File with name \"{fileName}\" not found.");

            lsfh = new LocalStorageFileHelper("Files");
            act2 = () => lsfh.GetRegistrationFileAsync("InvalidFileName.sst").Result;
            act2.Should().Throw<FileNotFoundException>().WithMessage($"File with name \"InvalidFileName.sst\" not found.");

            var readFile = lsfh.GetRegistrationFileAsync(fileName).Result;
            readFile.Should()
                .NotBeNull()
                .And
                .HaveCount(11)
                .And
                .BeEquivalentTo(file);
        }
    }
}
