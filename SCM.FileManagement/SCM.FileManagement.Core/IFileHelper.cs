﻿using CheckedException.Core;
using SCM.FileManagement.Core.Exceptions;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace SCM.FileManagement.Core
{
    [ThrowsException(typeof(FileManagementException))]
    public interface IFileHelper
    {
        Task<string> SaveFileAsync(byte[] file, string extension, FileOwnerModule module,
            CancellationToken cancellationToken = default);

        Task<string> SaveRegistrationFileAsync(byte[] file, string extension,
            CancellationToken cancellationToken = default);

        Task<string> SaveBillingFileAsync(byte[] file, string extension,
            CancellationToken cancellationToken = default);

        [ThrowsException(typeof(FileNotFoundException))]
        Task<byte[]> GetFileAsync(string fileName, FileOwnerModule module,
            CancellationToken cancellationToken = default);

        [ThrowsException(typeof(FileNotFoundException))]
        Task<byte[]> GetRegistrationFileAsync(string fileName,
            CancellationToken cancellationToken = default);

        [ThrowsException(typeof(FileNotFoundException))]
        Task<byte[]> GetBillingFileAsync(string fileName,
            CancellationToken cancellationToken = default);
    }
}
