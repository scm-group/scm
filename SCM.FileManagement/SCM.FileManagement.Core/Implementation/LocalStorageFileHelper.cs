﻿using CheckedException.Core;
using SCM.FileManagement.Core.Exceptions;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace SCM.FileManagement.Core.Implementation
{

    [ThrowsException(typeof(LocalStorageException))]
    public class LocalStorageFileHelper : IFileHelper
    {
        private readonly string _directory;

        public LocalStorageFileHelper(string directory)
        {
            _directory = directory;
        }

        [ThrowsException(typeof(FileNotFoundException))]
        public Task<byte[]> GetBillingFileAsync(string fileName, CancellationToken cancellationToken = default)
        {
            return GetFileAsync(fileName, FileOwnerModule.Bill);
        }

        [ThrowsException(typeof(FileNotFoundException))]
        public Task<byte[]> GetFileAsync(string fileName, FileOwnerModule module,
            CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrEmpty(_directory))
                throw new LocalStorageException($"Invalid directory: \"{_directory}\"");

            if (string.IsNullOrEmpty(fileName))
                throw new LocalStorageException("File name cannot be null or empty.");

            if (!Directory.Exists(_directory))
                throw new FileNotFoundException($"File with name \"{fileName}\" not found.");

            var specificDirectory = Path.Combine(_directory, module.GetEnumDescription());
            if (!Directory.Exists(specificDirectory))
                throw new FileNotFoundException($"File with name \"{fileName}\" not found.");
            
            var filePath = Path.Combine(specificDirectory, fileName);
            if(!File.Exists(filePath))
                throw new FileNotFoundException($"File with name \"{fileName}\" not found.");

            try
            {
                return File.ReadAllBytesAsync(filePath, cancellationToken);
            }
            catch(Exception ex)
            {
                throw new LocalStorageException("An error occurred when reading the file.", ex);
            }
        }

        [ThrowsException(typeof(FileNotFoundException))]
        public Task<byte[]> GetRegistrationFileAsync(string fileName, CancellationToken cancellationToken = default)
        {
            return GetFileAsync(fileName, FileOwnerModule.Registration);
        }

        public Task<string> SaveBillingFileAsync(byte[] file, string extension,
            CancellationToken cancellationToken = default)
        {
            return SaveFileAsync(file, extension, FileOwnerModule.Bill, cancellationToken);
        }

        public async Task<string> SaveFileAsync(byte[] file, string extension, FileOwnerModule module,
            CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrEmpty(_directory))
                throw new LocalStorageException($"Invalid directory: \"{_directory}\"");

            if (file == null || file.Length == 0)
                throw new LocalStorageException("Input file cannot be null or empty.");

            if (string.IsNullOrEmpty(extension) ||
                string.IsNullOrWhiteSpace(extension) ||
                extension == ".")
                throw new LocalStorageException($"Invalid file extension: \"{extension}\"");

            try
            {
                var specificDirectory = Path.Combine(_directory, module.GetEnumDescription());
                if (!Directory.Exists(specificDirectory))
                    Directory.CreateDirectory(specificDirectory);

                while (extension.StartsWith("."))
                    extension = extension.Substring(1);

                var date = DateTime.Now;
                var fileName = $"{module.GetEnumDescription()}_{date.Year:0000}{date.Month:00}{date.Day}_{date.Hour:00}{date.Minute:00}_{new Random(date.Millisecond).Next(0, 999999):000000}.{extension}";
                var filePath = Path.Combine(specificDirectory, fileName);
                using var fs = new FileStream(filePath, FileMode.CreateNew);
                await fs.WriteAsync(file, 0, file.Length);
                await fs.FlushAsync();

                return fileName;
            }
            catch(Exception ex)
            {
                throw new LocalStorageException("An exception occurred when saving the file.", ex);
            }
        }

        public Task<string> SaveRegistrationFileAsync(byte[] file, string extension,
            CancellationToken cancellationToken = default)
        {
            return SaveFileAsync(file, extension, FileOwnerModule.Registration, cancellationToken);
        }
    }
}
