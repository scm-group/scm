﻿using System;
using System.Runtime.Serialization;

namespace SCM.FileManagement.Core.Exceptions
{
    public class FileManagementException : Exception
    {
        public FileManagementException()
        {
        }

        public FileManagementException(string message) : base(message)
        {
        }

        public FileManagementException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected FileManagementException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
