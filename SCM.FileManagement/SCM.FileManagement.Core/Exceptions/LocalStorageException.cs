﻿using System;
using System.Runtime.Serialization;

namespace SCM.FileManagement.Core.Exceptions
{
    public class LocalStorageException : FileManagementException
    {
        public LocalStorageException()
        {
        }

        public LocalStorageException(string message) : base(message)
        {
        }

        public LocalStorageException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected LocalStorageException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
