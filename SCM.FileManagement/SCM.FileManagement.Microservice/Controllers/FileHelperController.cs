﻿using Microsoft.AspNetCore.Mvc;
using SCM.FileManagement.Core;
using SCM.FileManagement.Microservice.Models;
using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace SCM.FileManagement.Microservice.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class FileHelperController : ControllerBase
    {
        private readonly Serilog.ILogger _logger;
        private readonly IFileHelper _fileHelper;

        public FileHelperController(Serilog.ILogger logger,
            IFileHelper fileHelper)
        {
            _logger = logger;
            _fileHelper = fileHelper;
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(JsonResult), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> SaveBillingFile([FromBody] FileSaveModel model, 
            CancellationToken cancellationToken = default)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            try
            {
                _logger.Debug($"Saving a Billing file. Length = {(string.IsNullOrEmpty(model.Base64FileContent) ? "null" : model.Base64FileContent.Length.ToString())}, Extension = {model.Extension}");

                var file = Convert.FromBase64String(model.Base64FileContent);
                var fileName = await _fileHelper.SaveBillingFileAsync(file, model.Extension, cancellationToken);
                return Ok(new { SavedFileName = fileName });
            }
            catch (Core.Exceptions.FileManagementException ex)
            {
                _logger.Warning(ex, "A known exception has been occurred when saving the billing file.");
                return BadRequest(ex.Message);
            }
            catch(Exception ex)
            {
                _logger.Error(ex, "An unknown exception has been occurred when saving the billing file.");
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(JsonResult), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> SaveRegistrationFile([FromBody] FileSaveModel model,
            CancellationToken cancellationToken = default)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            try
            {
                _logger.Debug($"Saving a Registration file. Length = {(string.IsNullOrEmpty(model.Base64FileContent) ? "null" : model.Base64FileContent.Length.ToString())}, Extension = {model.Extension}");

                var file = Convert.FromBase64String(model.Base64FileContent);
                var fileName = await _fileHelper.SaveRegistrationFileAsync(file, model.Extension, cancellationToken);
                return Ok(new { SavedFileName = fileName });
            }
            catch (Core.Exceptions.FileManagementException ex)
            {
                _logger.Warning(ex, "A known exception has been occurred when saving the Registration file.");
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "An unknown exception has been occurred when saving the Registration file.");
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(byte[]),(int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetBillingFile(string fileName,
            CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrEmpty(fileName) ||
                !fileName.Contains('.'))
                return BadRequest("File name is null or invalid.");

            _logger.Debug("Getting a Billing file. FIle name = {fileName}", fileName);
            try
            {
                var file = await _fileHelper.GetBillingFileAsync(fileName, cancellationToken);
                return File(file, "application/octet-stream");
            }
            catch (FileNotFoundException)
            {
                _logger.Debug("File with name {fileName} not found.", fileName);
                return NotFound($"File with name \"{fileName}\" not found.");
            }
            catch (Core.Exceptions.FileManagementException ex)
            {
                _logger.Warning(ex, "A known exception has been occurred when getting the billing file.");
                return BadRequest(ex.Message);
            }
            catch(Exception ex)
            {
                _logger.Warning(ex, "A unknown exception has been occurred when getting the billing file.");
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(byte[]), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetRegistrationFile(string fileName,
            CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrEmpty(fileName) ||
                !fileName.Contains('.'))
                return BadRequest("File name is null or invalid.");

            _logger.Debug("Getting a Registration file. FIle name = {fileName}", fileName);
            try
            {
                var file = await _fileHelper.GetRegistrationFileAsync(fileName, cancellationToken);
                return File(file, "application/octet-stream");
            }
            catch (FileNotFoundException)
            {
                _logger.Debug("File with name {fileName} not found.", fileName);
                return NotFound($"File with name \"{fileName}\" not found.");
            }
            catch (Core.Exceptions.FileManagementException ex)
            {
                _logger.Warning(ex, "A known exception has been occurred when getting the Registration file.");
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.Warning(ex, "A unknown exception has been occurred when getting the Registration file.");
                return BadRequest(ex.Message);
            }
        }
    }
}
