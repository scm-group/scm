﻿using System.ComponentModel.DataAnnotations;

namespace SCM.FileManagement.Microservice.Models
{
    public class FileSaveModel
    {
        [Required(ErrorMessage = "File content is not defined.")]
        public string Base64FileContent { get; set; }

        [MaxLength(20, ErrorMessage = "The file extension length cannot be longer than 20 characters.")]
        [MinLength(1, ErrorMessage = "The file extension length cannot be smaller than 1 character.")]
        [Required(ErrorMessage = "The file extension is not defined.")]
        public string Extension { get; set; }
    }
}