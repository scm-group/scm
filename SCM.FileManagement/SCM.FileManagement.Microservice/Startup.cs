using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SCM.FileManagement.Core;
using SCM.FileManagement.Core.Implementation;
using SCM.FileManagement.Microservice.Models;
using Microsoft.OpenApi.Models;
using Serilog;
using Microsoft.Extensions.Logging;
using MassTransit;
using GreenPipes;
using SCM.FileManagement.Microservice.Infrastructure.Consumers;

namespace SCM.FileManagement.Microservice
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            Console.WriteLine("Starting the SCM.FileManagement.Microservice...");

            services.Configure<AppSettings>(Configuration);
            var appSettings = Configuration.Get<AppSettings>();

            // Setting up the HTTPS
            services.Configure<MvcOptions>(options =>
            {
                if (!appSettings.SkipHttps)
                {
                    options.Filters.Add(new RequireHttpsAttribute());
                }
            });

            if (appSettings.Configuration.StorageType.ToLower() == "local")
                services.AddTransient<IFileHelper,
                    LocalStorageFileHelper>(f => new LocalStorageFileHelper(appSettings.Configuration.Local.Directory));
            else
            {
                Console.Error.WriteLine($"Invalid data found for StorageType in Configuration. {appSettings.Configuration.StorageType}");
                throw new Exception($"Invalid data found for StorageType in Configuration. {appSettings.Configuration.StorageType}");
            }

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "FileManagement Service", Version = "v1" });
            });

            services.AddSingleton<Serilog.ILogger>(f =>
            {
                var logConf = new LoggerConfiguration()
                    .ReadFrom.Configuration(Configuration)
                    .Enrich.FromLogContext()
                    .WriteTo.File(appSettings.Serilog.LogFileName,
                                  restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Error,
                                  flushToDiskInterval: TimeSpan.FromMinutes(1),
                                  rollOnFileSizeLimit: true,
                                  encoding: System.Text.Encoding.UTF8,
                                  outputTemplate: "{NewLine}{Timestamp:HH:mm:ss} [{Level}] ({CorrelationToken}) {Message}{NewLine}{Exception}");

                return logConf.CreateLogger();
            });

            services.AddControllers();

            services.AddMassTransit(x =>
            {
                x.AddConsumer<SaveFileConsumer>();

                x.AddBus(context => Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    // configure health checks for this bus instance
                    cfg.UseHealthCheck(context);

                    cfg.Host(appSettings.Configuration.RabbitMq.Host);

                    cfg.ReceiveEndpoint("SaveFileRequest", ep =>
                    {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(r => r.Interval(3, TimeSpan.FromSeconds(5)));

                        ep.ConfigureConsumer<SaveFileConsumer>(context);
                    });
                }));
            });

            services.AddMassTransitHostedService();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            loggerFactory.AddSerilog();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "FileManagement V1");
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            Console.WriteLine("SCM.FileManagement.Microservice started.");
        }
    }
}
