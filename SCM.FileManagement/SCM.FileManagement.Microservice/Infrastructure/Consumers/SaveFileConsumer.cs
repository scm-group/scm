﻿using MassTransit;
using System.Threading.Tasks;

namespace SCM.FileManagement.Microservice.Infrastructure.Consumers
{
    public class SaveFileConsumer : IConsumer<SCM.Coordinator.Core.ConsumerModels.FileSaveModel>
    {
        public Task Consume(ConsumeContext<Coordinator.Core.ConsumerModels.FileSaveModel> context)
        {
            System.Diagnostics.Debug.WriteLine($"File received. Size: {context.Message.File.Length}", "Save file");
            return Task.CompletedTask;
        }
    }
}
