﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SCM.AccountManagement.Core;
using SCM.AccountManagement.Core.Models;
using SCM.AccountManagement.Microservice.Models;
using SCM.AccountManagement.Model;
using SCM.AccountManagement.Model.ViewModels.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace SCM.AccountManagement.Microservice.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class AccountController : ControllerBase
    {
        private IAccountManagement _accountManagement;
        public AccountController(IAccountManagement accountManagement)
        {
            _accountManagement = accountManagement;
        }

        //public IActionResult CheckPhoneNumber(string phoneNumber)
        //{
        //    bool isExsit = _accountManagement.IsExsitPhoneNumber(phoneNumber);
        //    if (isExsit)
        //        return Ok("User Exist");
        //    else
        //        return NotFound("User Not Exist");
        //}
        [HttpPost]
        public IActionResult CheckPhoneNumber([FromBody]PhoneNumberValidationModel model)
        {
            if (model == null)
                return BadRequest(new ResponseMessageModel{ Message = "Phonenumber cannot be null or empty text." });
            
            if (!model.PhoneNumber.IsValidCellPhoneNumber())
                return BadRequest(new ResponseMessageModel { Message = "The input phone number is not valid. The valid phone number should start with +989 or 09 and maximum length of the phone number is 13 characters." });
            
            bool isExsit = _accountManagement.IsExsitPhoneNumber(model.PhoneNumber);
            if (isExsit)
                return Ok(new ResponseMessageModel { Message = "Phonenumber found.", Status = true });
            else
                return Ok(new ResponseMessageModel { Message = "Phonenumber cannot be found.", Status = false });
        }

        [HttpPost]
        public IActionResult CheckEmail([FromBody]EmailValidationModel model)
        {
            if (model == null)
                return BadRequest(new ResponseMessageModel { Message = "Email cannot be null or empty text." });

            if (!model.Email.IsValidEmail())
                return BadRequest(new ResponseMessageModel { Message = "The input email is not valid. The valid email should start with +989 or 09 and maximum length of the phone number is 13 characters." });

            bool isExsit = _accountManagement.IsExsitEmail(model.Email);
            if (isExsit)
                return Ok(new ResponseMessageModel { Message = "Email found.", Status = true });
            else
                return Ok(new ResponseMessageModel { Message = "Email cannot be found.", Status = false });
        }

        //public IActionResult GetUserByPhoneNumberPassword(string phoneNumber, string password)
        //{
        //    var user = _accountManagement.GetUserByPhoneNumberPassword(phoneNumber, password);
        //    if (user != null)
        //        return new ObjectResult(user);
        //    else
        //        return new NoContentResult();
        //}SignInManager
        [HttpPost]
        public async Task<IActionResult> CheckPhoneNumberPassword([FromBody]PhoneNumberPasswordValidationModel model)
        {
            if (model == null)
                return BadRequest(new ResponseMessageModel { Message = "Phonenumber & Password cannot be null." });

            if (!model.PhoneNumber.IsValidCellPhoneNumber())
                return BadRequest(new ResponseMessageModel { Message = "The input phone number is not valid. The valid phone number should start with +989 or 09 and maximum length of the phone number is 13 characters." });

            if (!model.Password.IsValidParssword())
                return BadRequest(new ResponseMessageModel { Message = "The input password is not valid. The valid password should minimum 5 and maximum 100 characters, at least one uppercase letter, one lowercase letter, one number and one special character" });

            bool isExsit = await _accountManagement.IsExsitPhoneNumberPassword(model.PhoneNumber, model.Password);
            if (isExsit)
                return Ok();
            else
                return NotFound(new ResponseMessageModel { Message = "user cannot be found." });
        }

        [HttpPost]
        public IActionResult CheckUserIsActive([FromBody]PhoneNumberValidationModel model)
        {
            if (model == null)
                return BadRequest(new ResponseMessageModel { Message = "Phonenumber cannot be null." });

            var user = _accountManagement.GetUserByPhoneNumber(model.PhoneNumber);
            if (user == null)
                return NotFound(new ResponseMessageModel { Message = "user cannot be found." });

            if (user.IsActive)
                return Ok();
            else
                return NotFound(new ResponseMessageModel { Message = "user isnot be active." });
        }

        [HttpPost]
        public IActionResult GetUserByPhoneNumber([FromBody]PhoneNumberValidationModel model)
        {
            if (model == null)
                return BadRequest(new ResponseMessageModel { Message = "Phonenumber & Password cannot be null." });

            var user = _accountManagement.GetUserByPhoneNumber(model.PhoneNumber);
            if (user != null)
                return Ok(user);
                //return new ObjectResult(user);
            else
                return NotFound(new ResponseMessageModel { Message = "user cannot be found." });
                //return new NoContentResult();
        }

        [HttpPost]
        public async Task<IActionResult> InsertLegalUser([FromBody]LegalUserModel model)
        {
            if (model == null)
                return BadRequest(new ResponseMessageModel { Message = "model cannot be null." });

            bool status = await _accountManagement.AddLegalUser(model);
            if (status == true)
                return Ok();
            else
                return NotFound(new ResponseMessageModel { Message = "user dont register." });
        }

        [HttpPost]
        public IActionResult GetAllUser()
        {
            var users = _accountManagement.GetAllUser();
            //if (users.Count() > 0)
            if (users.Any())
                return Ok(users);
            //return new ObjectResult(user);
            else
                return NotFound(new ResponseMessageModel { Message = "user cannot be found." });
            //return new NoContentResult();
        }

        #region TUTORIAL
        //[HttpGet]
        //[HttpPost]
        //[HttpDelete]
        //[HttpPut]
        public IActionResult GetUser(string part)
        {
            try
            {
                //public List<TblUser> GetUser()
                //return (_accountManagement.GetAllUser());

                //Ok: HTTP Status Code = 200
                // StatusCode: Ok, ResponseStatus: Error, StatusDescription: Ok, IsSuccessful: false
                if (part == "Ok")
                {
                    return Ok("User name is OK");
                }
                //BadRequest: HTTP Status Code = 400
                // StatusCode: BadRequest, ResponseStatus: Error, StatusDescription: Bad Request, IsSuccessful: false
                else if (part == "BadRequest")
                {
                    return BadRequest("User name is NOT OK");
                }
                //BadRequest: HTTP Status Code = 404
                // StatusCode: NotFound, ResponseStatus: Error, StatusDescription: Not Found, IsSuccessful: false
                else if (part == "NotFound")
                {
                    return NotFound("User name is NOT Found");
                }
                //NoContentResult: HTTP Status Code = 200
                // StatusCode: NoContent, ResponseStatus: Completed, StatusDescription: No Content, IsSuccessful: true
                else if (part == "NoContentResult")
                {
                    //if model data is empty
                    return new NoContentResult();
                }
                //ObjectResult: HTTP Status Code = 200
                // StatusCode: OK, ResponseStatus: Completed, StatusDescription: OK, IsSuccessful: true
                else
                {
                    List<TblUser> user = _accountManagement.GetAllUser();
                    return new ObjectResult(user);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            //return _accountManagement.GetAllUser();
        }
        #endregion
    }
}
