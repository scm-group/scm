﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace SCM.AccountManagement.Microservice.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HealthCheckController: ControllerBase
    {
        [HttpGet]
        public IActionResult Index()
        {
            return Ok(new 
            { 
                RequestTime = DateTime.Now, 
                Status = "Ok", 
                Desc = $"FileManager Service is up and running. Go to {Request.Scheme}://{Request.Host.Value}/swagger to see the methods."
            });
        }
    }
}
