﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SCM.AccountManagement.Microservice.Models
{
    public class EmailValidationModel
    {
        public string Email { get; set; }
    }
}
