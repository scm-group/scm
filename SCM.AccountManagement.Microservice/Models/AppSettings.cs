﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SCM.FileManagement.Microservice.Models
{
    public partial class AppSettings
    {
        [JsonProperty("Logging")]
        public Logging Logging { get; set; }

        [JsonProperty("ConnectionString")]
        public string ConnectionString { get; set; }

        [JsonProperty("SkipHTTPS")]
        public bool SkipHttps { get; set; }
    }

    public partial class Logging
    {
        [JsonProperty("LogLevel")]
        public LogLevel LogLevel { get; set; }
    }

    public partial class LogLevel
    {
        [JsonProperty("Default")]
        public string Default { get; set; }

        [JsonProperty("Microsoft")]
        public string Microsoft { get; set; }

        [JsonProperty("Microsoft.Hosting.Lifetime")]
        public string MicrosoftHostingLifetime { get; set; }
    }
}
