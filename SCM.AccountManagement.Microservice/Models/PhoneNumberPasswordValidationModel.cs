﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SCM.AccountManagement.Microservice.Models
{
    public class PhoneNumberPasswordValidationModel
    {
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
    }
}
