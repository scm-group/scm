﻿namespace SCM.AccountManagement.Microservice.Models
{
    public class ResponseMessageModel
    {
        public string Message { get; set; }
        public bool Status { get; set; }
    }
}
