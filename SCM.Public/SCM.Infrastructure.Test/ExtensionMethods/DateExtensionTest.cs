﻿using FluentAssertions;
using SCM.Infrastructure.Exceptions;
using System;
using System.Globalization;
using Xunit;

namespace SCM.Infrastructure.Test.ExtensionMethods
{
    public class DateExtensionTest
    {
        [Fact]
        [CheckedException.Core.ThrowsException(typeof(DateExtensionException))]
        [CheckedException.Core.ThrowsException(typeof(ArgumentOutOfRangeException))]
        public void GetShamsiYearTest()
        {
            Func<int> act = () => default(DateTime).GetShamsiYear();
            act.Should().Throw<ArgumentOutOfRangeException>("Input date is not valid for Shamsi date.");

            act = () => new DateTime().GetShamsiYear();
            act.Should().Throw<ArgumentOutOfRangeException>("Input date is not valid for Shamsi date.");

            PersianCalendar pc = new PersianCalendar();
            DateTime.Now.GetShamsiYear().Should().Be(pc.GetYear(DateTime.Now));

            new DateTime(2020, 1, 1).GetShamsiYear().Should().Be(1398);
            new DateTime(2019, 12, 31).GetShamsiYear().Should().Be(1398);

            new DateTime(2020, 3, 19).GetShamsiYear().Should().Be(1398);
            new DateTime(2020, 3, 20).GetShamsiYear().Should().Be(1399);
        }

        [Fact]
        [CheckedException.Core.ThrowsException(typeof(ArgumentOutOfRangeException))]
        public void GetShamsiMonthTest()
        {
            Func<int> act = () => default(DateTime).GetShamsiMonth();
            act.Should().Throw<ArgumentOutOfRangeException>("Input date is not valid for Shamsi date.");

            act = () => new DateTime().GetShamsiMonth();
            act.Should().Throw<ArgumentOutOfRangeException>("Input date is not valid for Shamsi date.");

            PersianCalendar pc = new PersianCalendar();
            DateTime.Now.GetShamsiMonth().Should().Be(pc.GetMonth(DateTime.Now));

            new DateTime(2020, 1, 1).GetShamsiMonth().Should().Be(10);
            new DateTime(2019, 12, 31).GetShamsiMonth().Should().Be(10);

            new DateTime(2020, 3, 19).GetShamsiMonth().Should().Be(12);
            new DateTime(2020, 3, 20).GetShamsiMonth().Should().Be(1);
        }

        [Fact]
        [CheckedException.Core.ThrowsException(typeof(ArgumentOutOfRangeException))]
        public void GetShamsiDayTest()
        {
            Func<int> act = () => default(DateTime).GetShamsiDay();
            act.Should().Throw<ArgumentOutOfRangeException>("Input date is not valid for Shamsi date.");

            act = () => new DateTime().GetShamsiDay();
            act.Should().Throw<ArgumentOutOfRangeException>("Input date is not valid for Shamsi date.");

            PersianCalendar pc = new PersianCalendar();
            DateTime.Now.GetShamsiDay().Should().Be(pc.GetDayOfMonth(DateTime.Now));

            new DateTime(2020, 1, 1).GetShamsiDay().Should().Be(11);
            new DateTime(2019, 12, 31).GetShamsiDay().Should().Be(10);

            new DateTime(2020, 3, 19).GetShamsiDay().Should().Be(29);
            new DateTime(2020, 3, 20).GetShamsiDay().Should().Be(1);
        }
    }
}
