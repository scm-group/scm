﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SCM.Infrastructure.Utility
{
    public static class DigitConvertor
    {
        public static string PersianToEnglish(string Date)
        {
            Dictionary<string, string> LettersDictionary = new Dictionary<string, string>
            {
                ["۰"] = "0",
                ["۱"] = "1",
                ["۲"] = "2",
                ["۳"] = "3",
                ["۴"] = "4",
                ["۵"] = "5",
                ["۶"] = "6",
                ["۷"] = "7",
                ["۸"] = "8",
                ["۹"] = "9"
            };

            return LettersDictionary.Aggregate(Date, (current, item) => current.Replace(item.Key, item.Value));
        }
    }
}
