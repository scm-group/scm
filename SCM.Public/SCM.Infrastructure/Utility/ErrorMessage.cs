﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCM.Infrastructure.Utility
{
    public class ErrorMessage
    {
        public const string RequiredMsg = "{0} را وارد نمایید";
        public const string StringLengthMsg = "{0} باید بین {2} تا {1} باشد";
        public const string RangeMsg = "{0} باید بین {2} تا {1} باشد";
        public const string MaxLengthMsg = "{0} نباید بیش از {1} کاراکتر باشد";
        public const string MinLengthMsg = "{0} نباید کمتر از {1} کاراکتر باشد";
        public const string RegularExpressionMsg = "{0} معتبر نمی باشد";
        public const string FormatMsg = "{0} معتبر نمی باشد";
        public const string ConfirmPasswordMsg = "{0} با تکرار آن برابر نمی باشد";
        public const string CompareLengthMsg = "{0} با {1} تطبیق ندارد";
        public const string DuplicateMsg = "{0} تکراری است";
        public const string ExistMsg = "{0} ثبت نشده است";
    }
}
