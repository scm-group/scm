﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SCM.Infrastructure.Utility
{
    public class CheckFileAttribute : ValidationAttribute
    {
        private long _MaxSize { get; set; }
        private string _ContentType { get; set; }
        private bool _IsRequired { get; set; }

        public CheckFileAttribute(bool isRequired, int MaxSize, string ContentType)
        {
            _MaxSize = MaxSize;
            _ContentType = ContentType;
            _IsRequired = isRequired;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            IFormFile MyFile = (IFormFile)value;
            if (_IsRequired == true && MyFile == null)
            {
                return new ValidationResult("فایل را انتخاب نمایید");
            }

            if (MyFile != null)
            {
                if (MyFile.Length > (_MaxSize * 1024))
                {
                    return new ValidationResult(" حجم فایل نباید بیش از " + (_MaxSize) + " باشد ");
                }

                if (!_ContentType.Contains(MyFile.ContentType))
                {
                    return new ValidationResult("فایل معتبر نمی باشد");
                }
            }
            else
            {
                return null;
            }

            return null;
        }
    }
}
