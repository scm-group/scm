﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SCM.Infrastructure.Utility
{
    public class RegularExpressionFormat
    {
        public const string EmailRegularExpression = @"[A-Za-z0-9_\-\.]{3,100}[@][A-Za-z0-9_\-\.]{3,100}[\.][A-Za-z0-9_\-\.]{2,50}";
        public const string TitleRegularExpression = @"[0-9۰-۹A-Zا-یa-z_آءئ\s\n\t\(\)\[\]\{\}\<\>\?\!\,\؟\،\-\+\.\=\:\;\'/\\]+";
        public const string EnglishRegularExpression = @"[A-Za-z0-9_\-\.]+";
        public const string TextRegularExpression = @"[0-9A-Zا-یa-z_آ\s\n\t\(\)\-\.\?\!\,\؟\،\=\:]+";
        public const string PhoneRegularExpression = @"[0-9\-\+]+";
        public const string NumberRegularExpression = @"[0-9]+";
        //public const string NumberRegularExpression = @"[0-9\-]+";
        public const string PasswordRegularExpression = @"[A-Za-z0-9_\(\)\[\]\{\}\<\>\?\!\,\-\+\.\=\:\;\'\@\#\$\%\&\*\|\~\`\^/\\]+";
        public const string NameRegularExpression = @"[A-Zا-یa-z_آءئ\s\n\t]+";
        public const string DateRegularExpression = @"[0-9]{4}[/]{1}[0-9]{2}[/]{1}[0-9]{2}"; // 1366/06/28
        public const string DatePersianRegularExpression = @"[۰-۹]{4}[/]{1}[۰-۹]{2}[/]{1}[۰-۹]{2}"; // ۱۳۹۹/۰۶/۲۸
        public const string TimeRegularExpression = @"[0-9]{2}[:]{1}[0-9]{2}[\s\n\t]{1}[-]{1}[\s\n\t]{1}[0-9]{2}[:]{1}[0-9]{2}"; // 08:30 - 09:00
        public const string UrlRegularExpression = @"https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)";
        public const string PhoneNumberRegularExpression = @"09[0-9]{9}"; // 09125261762
        public const string VerificationRegularExpression = @"[0-9]{6}"; // 123456
        public const string UserTypeRegularExpression = @"[حقیقی|حقوقی]"; // حقیقی | حقوقی
    }
}
