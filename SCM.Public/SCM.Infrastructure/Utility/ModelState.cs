﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SCM.Infrastructure.Utility
{
    public static class ModelState
    {
        public static string GetErrors(this ModelStateDictionary modelState)
        {
            //return string.Join("<br/>", (
            return string.Join("|", (
                from item in modelState
                where item.Value.Errors.Any()
                select item.Value.Errors[0].ErrorMessage).ToList()
                );
        }
    }
}
