﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace SCM.Infrastructure.Utility
{
    public static class DateConvertor
    {
        // 1366 shahrivar 1
        public static string GetStringDate(DateTime dt)
        {
            PersianCalendar pDate = new PersianCalendar();

            int Year = pDate.GetYear(dt);
            int Month = pDate.GetMonth(dt);
            int Day = pDate.GetDayOfMonth(dt);

            string[] Months = new string[] { "فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند" };

            return Day + " " + Months[Month - 1] + " " + Year;
        }

        // 15:24:30
        public static string GetStringTime(DateTime dt)
        {
            PersianCalendar pDate = new PersianCalendar();

            int Hour = pDate.GetHour(dt);
            int Minute = pDate.GetMinute(dt);
            int Second = pDate.GetSecond(dt);

            return Hour + ":" + Minute + ":" + Second;
        }

        // 15
        public static int GetIntHour(DateTime dt)
        {
            PersianCalendar pDate = new PersianCalendar();

            int Hour = pDate.GetHour(dt);

            return Hour;
        }

        // 1366 shahrivar 1 | 15:24
        public static string GetStringDateTime(DateTime dt)
        {
            PersianCalendar pDate = new PersianCalendar();

            int Year = pDate.GetYear(dt);
            int Month = pDate.GetMonth(dt);
            int Day = pDate.GetDayOfMonth(dt);
            int Hour = pDate.GetHour(dt);
            int Minute = pDate.GetMinute(dt);

            string[] Months = new string[] { "فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند" };

            return Day + " " + Months[Month - 1] + " " + Year + " | " + Hour + ":" + Minute;
        }

        // 1366/06/01
        public static string GetStringDateWithSlash(DateTime dt)
        {
            PersianCalendar pDate = new PersianCalendar();

            int Year = pDate.GetYear(dt);
            string Month = pDate.GetMonth(dt).ToString();
            string Day = pDate.GetDayOfMonth(dt).ToString();

            // jahate inke age adade tak raghami dashtim, 2 raghamishun konim
            if (Month.Length == 1)
                Month = "0" + Month;
            if (Day.Length == 1)
                Day = "0" + Day;

            return Year + "/" + Month + "/" + Day;
        }

        // jome | 1366 shahrivar 1 | 15:24
        public static string GetStringDateWeek(DateTime dt)
        {
            PersianCalendar pDate = new PersianCalendar();

            int Year = pDate.GetYear(dt);
            int Month = pDate.GetMonth(dt);
            int Day = pDate.GetDayOfMonth(dt);
            string Week = dt.DayOfWeek.ToString();
            int Hour = pDate.GetHour(dt);
            int Minute = pDate.GetMinute(dt);
            int Second = pDate.GetSecond(dt);

            string[] Months = new string[] { "فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند" };

            if (Week == "Saturday")
                Week = "شنبه";
            if (Week == "Sunday")
                Week = "یک شنبه";
            if (Week == "Monday")
                Week = "دو شنبه";
            if (Week == "Tuesday")
                Week = "سه شنبه";
            if (Week == "Wednesday")
                Week = "چهار شنبه";
            if (Week == "Thursday")
                Week = "پنج شنبه";
            if (Week == "Friday")
                Week = "جمعه";

            return Week + " | " + Day + " " + Months[Month - 1] + " " + Year + " | " + Hour + ":" + Minute;
        }

        // 12 rouz pish
        public static string GetStringDateWeekNoTime(DateTime dt)
        {
            PersianCalendar pDate = new PersianCalendar();

            int Year = pDate.GetYear(dt);
            int Month = pDate.GetMonth(dt);
            int Day = pDate.GetDayOfMonth(dt);
            string Week = dt.DayOfWeek.ToString();

            string[] Months = new string[] { "فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند" };

            if (Week == "Saturday")
                Week = "شنبه";
            if (Week == "Sunday")
                Week = "یک شنبه";
            if (Week == "Monday")
                Week = "دو شنبه";
            if (Week == "Tuesday")
                Week = "سه شنبه";
            if (Week == "Wednesday")
                Week = "چهار شنبه";
            if (Week == "Thursday")
                Week = "پنج شنبه";
            if (Week == "Friday")
                Week = "جمعه";

            return Week + " | " + Day + " " + Months[Month - 1] + " " + Year;
        }

        public static string GetTime(DateTime dt)
        {
            TimeSpan ts = new TimeSpan();
            ts = (DateTime.Now - dt);

            string TimeName = "";

            if (ts.Days > 0)
            {
                if ((ts.Days / 30) >= 1)
                {
                    int Month = ts.Days / 30;
                    TimeName = Month + " ماه پیش";
                    return TimeName;
                }
                else
                {
                    int Day = ts.Days;
                    TimeName = ts.Days + " روز پیش";
                    return TimeName;
                }
            }

            if (ts.Hours > 0)
            {
                TimeName = ts.Hours + " ساعت پیش";
                return TimeName;
            }

            if (ts.Minutes > 0)
            {
                TimeName = ts.Minutes + " دقیقه پیش";
                return TimeName;
            }

            if (ts.Seconds > 0)
            {
                TimeName = ts.Seconds + " ثانیه پیش";
                return TimeName;
            }

            if (ts.Milliseconds > 0)
            {
                TimeName = ts.Milliseconds + " میلی ثانیه پیش";
                return TimeName;
            }

            return "";
        }

        public static DateTime ToMiladi(DateTime dt)
        {
            PersianCalendar pDate = new PersianCalendar();

            return pDate.ToDateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second, dt.Millisecond);
        }

        public static DateTime StringToDatetime(string pDate)
        {
            var dateParts = pDate.Split(new[] { '/' }).Select(d => int.Parse(d)).ToArray();
            var hour = 0;
            var min = 0;
            var seconds = 0;
            return new DateTime(dateParts[0], dateParts[1], dateParts[2], hour, min, seconds, new PersianCalendar());
        }
    }
}
