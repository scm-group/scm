﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace SCM.Infrastructure.Utility
{
    public class MultiLanguageErrorMessage : IdentityErrorDescriber
    {
        public override IdentityError DuplicateEmail(string email)
        {
            return new IdentityError()
            {
                Code = nameof(DuplicateEmail),
                Description = string.Format("{0} تکراری است", email)
            };
        }

        public override IdentityError DuplicateRoleName(string role)
        {
            return new IdentityError()
            {
                Code = nameof(DuplicateRoleName),
                Description = string.Format("سطح دسترسی با نام {0} تکراری است", role)
            };
        }

        public override IdentityError DuplicateUserName(string userName)
        {
            return new IdentityError()
            {
                Code = nameof(DuplicateUserName),
                Description = string.Format("{0} تکراری است", userName)
            };
        }

        public override IdentityError PasswordRequiresDigit()
        {
            return new IdentityError()
            {
                Code = nameof(PasswordRequiresDigit),
                Description = "رمز عبور باید حاوی اعداد باشد"
            };
        }

        public override IdentityError PasswordRequiresLower()
        {
            return new IdentityError()
            {
                Code = nameof(PasswordRequiresLower),
                Description = "رمز عبور باید حاوی کاراکتر های کوچک اینگلیسی باشد"
            };
        }

        public override IdentityError PasswordRequiresNonAlphanumeric()
        {
            return new IdentityError()
            {
                Code = nameof(PasswordRequiresLower),
                Description = "رمز عبور باید حاوی کاراکتر های غیر الفبایی (!@#...) باشد"
            };
        }

        public override IdentityError PasswordRequiresUniqueChars(int uniqueChars)
        {
            return new IdentityError()
            {
                Code = nameof(PasswordRequiresUniqueChars),
                Description = "رمز عبور باید حاوی کاراکتر های تکرار نشده به تعداد" + uniqueChars + " عدد باشد"
            };
        }

        public override IdentityError PasswordRequiresUpper()
        {
            return new IdentityError()
            {
                Code = nameof(PasswordRequiresUpper),
                Description = "رمز عبور باید حاوی کاراکتر های بزرگ اینگلیسی باشد"
            };
        }

        public override IdentityError PasswordTooShort(int length)
        {
            return new IdentityError()
            {
                Code = nameof(PasswordRequiresUpper),
                Description = "تعداد کاراکتر های رمز عبور باید حداقل " + length + " کاراکتر باشد"
            };
        }





        public override IdentityError InvalidEmail(string email)
            => new IdentityError()
            {
                Code = nameof(InvalidEmail),
                Description = $"یک ایمیل معتبر نیست '{email}' ایمیل"
            };

        public override IdentityError InvalidRoleName(string role)
            => new IdentityError()
            {
                Code = nameof(InvalidRoleName),
                Description = $"معتبر نیست '{role}' نام"
            };

        public override IdentityError InvalidUserName(string userName)
            => new IdentityError()
            {
                Code = nameof(InvalidUserName),
                Description = $"معتبر نیست '{userName}' نام کاربری"
            };

        public override IdentityError UserNotInRole(string role)
            => new IdentityError()
            {
                Code = nameof(UserNotInRole),
                Description = $"نیست '{role}' کاربر مورد نظر در مقام"
            };

        public override IdentityError UserAlreadyInRole(string role)
            => new IdentityError()
            {
                Code = nameof(UserAlreadyInRole),
                Description = $"است '{role}' کاربر مورد نظر در مقام"
            };

        public override IdentityError DefaultError()
            => new IdentityError()
            {
                Code = nameof(DefaultError),
                Description = "خطای پیشبینی نشده رخ داد"
            };

        public override IdentityError ConcurrencyFailure()
            => new IdentityError()
            {
                Code = nameof(ConcurrencyFailure),
                Description = "خطای همزمانی رخ داد"
            };

        public override IdentityError InvalidToken()
            => new IdentityError()
            {
                Code = nameof(InvalidToken),
                Description = "توکن معتبر نیست"
            };

        public override IdentityError RecoveryCodeRedemptionFailed()
            => new IdentityError()
            {
                Code = nameof(RecoveryCodeRedemptionFailed),
                Description = "کد بازیابی معتبر نیست"
            };

        public override IdentityError UserLockoutNotEnabled()
            => new IdentityError()
            {
                Code = nameof(UserLockoutNotEnabled),
                Description = "قابلیت قفل اکانت کاربر فعال نیست"
            };

        public override IdentityError UserAlreadyHasPassword()
            => new IdentityError()
            {
                Code = nameof(UserAlreadyHasPassword),
                Description = "کاربر از قبل رمزعبور دارد"
            };

        public override IdentityError PasswordMismatch()
            => new IdentityError()
            {
                Code = nameof(PasswordMismatch),
                Description = "عدم تطابق رمزعبور"
            };

        public override IdentityError LoginAlreadyAssociated()
            => new IdentityError()
            {
                Code = nameof(LoginAlreadyAssociated),
                Description = "از قبل اکانت خارجی به حساب این کاربر متصل اصت"
            };
    }
}
