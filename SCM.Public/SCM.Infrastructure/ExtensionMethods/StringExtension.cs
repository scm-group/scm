﻿using System.Text.RegularExpressions;

namespace System
{
    public static class StringExtension
    {
        public static string GetControllerName(this string value)
        {
            if (string.IsNullOrEmpty(value))
                throw new Exception("Controller name is null");

            var index = value.ToLower().IndexOf("controller");
            if (index < 0)
                return value;

            return value.Substring(0, index);
        }

        public static bool IsValidEmail(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;
            var regex = new Regex("^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])$");
            return regex.IsMatch(value);
        }

        public static bool IsValidCellPhoneNumber(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;
            Regex regex = new Regex(@"^(\+989|09)[0-9]{9}$");
            return regex.IsMatch(value);
        }

        public static bool IsValidParssword(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;
            //Regex regex = new Regex(@"[A-Za-z0-9_\(\)\[\]\{\}\<\>\?\!\,\-\+\.\=\:\;\'\@\#\$\%\&\*\|\~\`\^/\\]+");
            //Regex regex = new Regex(@"[A-Za-z0-9_\(\)\[\]\{\}\<\>\?\!\,\-\+\.\=\:\;\'\@\#\$\%\&\*\|\~\`\^/\\]{6,100}$");
            Regex regex = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{5,100}$");
            return regex.IsMatch(value);
        }
    }
}
