﻿using CheckedException.Core;
using SCM.Infrastructure.Exceptions;
using System;
using System.ComponentModel;
using System.Globalization;

namespace System
{
    public static class DateExtension
    {
        public enum ShamsiMonths
        {
            [Description("فروردین")]
            Farvardin = 1,
            [Description("اردیبهشت")]
            Ordibehesht,
            [Description("خرداد")]
            Khordad,
            [Description("تیر")]
            Tir,
            [Description("مرداد")]
            Mordad,
            [Description("شهریور")]
            Shahrivar,
            [Description("مهر")]
            Mehr,
            [Description("آبان")]
            Aban,
            [Description("آذر")]
            Azar,
            [Description("دی")]
            Day,
            [Description("بهمن")]
            Bahman,
            [Description("اسفند")]
            Esfand
        }

        public enum ShamsiWeekDays
        {
            [Description("یک شنبه")]
            Yekshanbe,
            [Description("دوشنبه")]
            Doshanbe,
            [Description("سه شنبه")]
            Seshanbe,
            [Description("چهارشنبه")]
            Chaharchanbe,
            [Description("پنج شنبه")]
            Panjshanbe,
            [Description("جمعه")]
            Jomee,
            [Description("شنبه")]
            Shanbe,
        }

        [ThrowsException(typeof(ArgumentOutOfRangeException))]
        public static int GetShamsiYear(this DateTime dateTime)
        {
            return new PersianCalendar().GetYear(dateTime);
        }

        [ThrowsException(typeof(ArgumentOutOfRangeException))]
        public static int GetShamsiMonth(this DateTime dateTime)
        {
            return new PersianCalendar().GetMonth(dateTime);
        }

        [ThrowsException(typeof(ArgumentOutOfRangeException))]
        public static int GetShamsiDay(this DateTime dateTime)
        {
            return new PersianCalendar().GetDayOfMonth(dateTime);
        }

        public static string ToPersianDate(this DateTime dateTime)
        {
            PersianCalendar pc = new PersianCalendar();
            return $"{pc.GetYear(dateTime)}/{pc.GetMonth(dateTime):00}/{pc.GetDayOfMonth(dateTime):00}";
        }

        public static string ToPersianDateTime(this DateTime dateTime)
        {
            PersianCalendar pc = new PersianCalendar();
            return $"{pc.GetYear(dateTime)}/{pc.GetMonth(dateTime):00}/{pc.GetDayOfMonth(dateTime):00} {dateTime.Hour}:{dateTime.Minute:00}";
        }

        public static string ToPersianLongDate(this DateTime dateTime)
        {
            var pc = new PersianCalendar();
            var ret = $"{((ShamsiWeekDays)pc.GetDayOfWeek(dateTime)).GetEnumDescription()}، {pc.GetDayOfMonth(dateTime)} {((ShamsiMonths)pc.GetMonth(dateTime)).GetEnumDescription()} {pc.GetYear(dateTime)}";
            return ret;
        }

        public static string ToPersianLongDateTime(this DateTime dateTime)
        {
            var pc = new PersianCalendar();
            var ret = $"{((ShamsiWeekDays)pc.GetDayOfWeek(dateTime)).GetEnumDescription()}، {pc.GetDayOfMonth(dateTime)} {((ShamsiMonths)pc.GetMonth(dateTime)).GetEnumDescription()} {pc.GetYear(dateTime)} ساعت {dateTime:HH:mm:ss}";
            return ret;
        }

        public static string GetShamsiMonthAndDay(this DateTime dateTime)
        {
            PersianCalendar pc = new PersianCalendar();
            return $"{pc.GetMonth(dateTime):00}/{pc.GetDayOfMonth(dateTime):00}";
        }

        public static DateTime GetShamsiMonthStart(this DateTime dateTime)
        {
            PersianCalendar pc = new PersianCalendar();
            return new DateTime(pc.GetYear(dateTime), pc.GetMonth(dateTime), 1, 0, 0, 0, pc);
        }

        public static DateTime GetShamsiMonthEnd(this DateTime dateTime)
        {
            PersianCalendar pc = new PersianCalendar();
            DateTime res = dateTime.AddMonths(1);
            res = res.AddDays(-1 * pc.GetDayOfMonth(res));
            res = new DateTime(res.Year, res.Month, res.Day, 23, 59, 59);
            return res;
        }

        public static DateTime ToLocalDateTime(this long timeInMillis)
        {
            DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var timeInNano = TimeSpan.TicksPerMillisecond * timeInMillis;
            var tmpDate = new TimeSpan(timeInNano);
            var result = Jan1st1970.Add(tmpDate).ToLocalTime();
            return result;
        }
    }
}
