﻿using System.ComponentModel;
using System.Linq;

namespace System
{
    public static class EnumExtension
    {
        public static string GetEnumDescription(this Enum value)
        {
            var type = value.GetType();
            var memInfo = type.GetMember(value.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes.Any())
                return ((DescriptionAttribute)attributes[0]).Description;
            else
                return value.ToString();
        }
    }
}
