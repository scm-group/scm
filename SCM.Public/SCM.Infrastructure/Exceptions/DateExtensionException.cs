﻿using System;
using System.Runtime.Serialization;

namespace SCM.Infrastructure.Exceptions
{
    public class DateExtensionException : InfrastructureException
    {
        public DateExtensionException()
        {
        }

        public DateExtensionException(string message) : base(message)
        {
        }

        public DateExtensionException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected DateExtensionException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
