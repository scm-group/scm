﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SCM.Coordinator.Microservice.Models
{
    public partial class AppSettings
    {
        [JsonProperty("Logging")]
        public Logging Logging { get; set; }

        [JsonProperty("Serilog")]
        public Serilog Serilog { get; set; }

        [JsonProperty("SkipHTTPS")]
        public bool SkipHttps { get; set; }

        [JsonProperty("AllowedHosts")]
        public string AllowedHosts { get; set; }

        [JsonProperty("Configuration")]
        public Configuration Configuration { get; set; }
    }    

    public partial class Configuration
    {
        [JsonProperty("RabbitMq")]
        public RabbitMq RabbitMq { get; set; }
    }

    public partial class Logging
    {
        [JsonProperty("LogLevel")]
        public LogLevel LogLevel { get; set; }
    }

    public partial class LogLevel
    {
        [JsonProperty("Default")]
        public string Default { get; set; }

        [JsonProperty("Microsoft")]
        public string Microsoft { get; set; }

        [JsonProperty("Microsoft.Hosting.Lifetime")]
        public string MicrosoftHostingLifetime { get; set; }
    }

    public partial class Serilog
    {
        [JsonProperty("MinimumLevel")]
        public MinimumLevel MinimumLevel { get; set; }

        [JsonProperty("Enrich")]
        public List<string> Enrich { get; set; }

        [JsonProperty("LogFileName")]
        public string LogFileName { get; set; }
    }

    public partial class MinimumLevel
    {
        [JsonProperty("Default")]
        public string Default { get; set; }

        [JsonProperty("Override")]
        public Override Override { get; set; }
    }

    public partial class Override
    {
        [JsonProperty("Microsoft")]
        public string Microsoft { get; set; }

        [JsonProperty("System")]
        public string System { get; set; }
    }

    public partial class RabbitMq
    {
        [JsonProperty("Host")]
        public string Host { get; set; }

        [JsonProperty("Username")]
        public string Username { get; set; }

        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonProperty("RetryTimeout")]
        public int RetryTimeout { get; set; }
    }
}
