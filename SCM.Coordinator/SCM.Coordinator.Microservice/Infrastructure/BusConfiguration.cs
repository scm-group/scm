﻿using MassTransit;
using Microsoft.Extensions.DependencyInjection;
using SCM.Coordinator.Microservice.Models;

namespace SCM.Coordinator.Microservice.Infrastructure
{
    public static class BusConfiguration
    {
        public static IServiceCollection AddBus(this IServiceCollection service, AppSettings appSettings)
        {
            service.AddMassTransit(x =>
            {
                x.AddBus(context => Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    cfg.Host($"{appSettings.Configuration.RabbitMq.Host}", f =>
                        {
                            f.Username(appSettings.Configuration.RabbitMq.Username);
                            f.Password(appSettings.Configuration.RabbitMq.Password);
                        });
                }));
            });

            service.AddMassTransitHostedService();
            return service;
        }
    }
}
