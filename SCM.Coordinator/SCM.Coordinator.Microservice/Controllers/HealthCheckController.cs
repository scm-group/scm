﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace SCM.FileManagement.Microservice.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HealthCheckController: ControllerBase
    {
        [HttpGet]
        public IActionResult Index()
        {
            return Ok(new 
            { 
                RequestTime = DateTime.Now, 
                Status = "Ok", 
                Desc = $"Coordinator Service is up and running. Go to {Request.Scheme}://{Request.Host.Value}/swagger to see the methods."
            });
        }
    }
}
