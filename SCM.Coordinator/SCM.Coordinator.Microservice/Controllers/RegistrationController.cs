﻿using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace SCM.Coordinator.Microservice.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RegistrationController : ControllerBase
    {
        private readonly IBusControl busControl;
        public RegistrationController(IBusControl busControl)
        {
            this.busControl = busControl;
        }

        [HttpGet]
        public async Task<IActionResult> RegisterUser()
        {
            var file = new Core.ConsumerModels.FileSaveModel
            {
                File = new byte[] { 10, 5, 7, 89, 54, 21, 54, 9, 87, 254, 21, 54, 87, 24, 21, 54, 8, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 5 },
                Module = FileManagement.Core.FileOwnerModule.Bill
            };
            await busControl.Publish(file);
            return Ok();
        }
    }
}
