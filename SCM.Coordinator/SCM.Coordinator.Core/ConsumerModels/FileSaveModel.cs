﻿using MassTransit;
using SCM.FileManagement.Core;
using System.Threading.Tasks;

namespace SCM.Coordinator.Core.ConsumerModels
{

    public class FileSaveModel
    {
        public FileOwnerModule Module { get; set; }

        public byte[] File { get; set; }

        public string FileName { get; set; }
    }
}
