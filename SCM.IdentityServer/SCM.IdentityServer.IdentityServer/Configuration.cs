﻿using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SCM.IdentityServer.IdentityServer
{
    public static class Configuration
    {
        public static IEnumerable<IdentityResource> GetIdentityResources() =>
            new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                //new IdentityResources.Profile(),
                new IdentityResource
                {
                    Name = "rc.scope",
                    UserClaims =
                    {
                        // dar program.cs, ye AddClaimAsync anjam dadim ke type esh "rc.garndma" ast
                        "rc.garndma"
                    }
                }
            };

        public static IEnumerable<ApiResource> GetApis() =>
            new List<ApiResource>
            {
                // dar startup e projeye ApiOne darim: config.Audience = "ApiOne"
                new ApiResource("ApiOne"),
                // dar startup e projeye ApiTwo darim: config.Audience = "ApiTwo"
                new ApiResource("ApiTwo",
                    // dar program.cs, ye AddClaimAsync anjam dadim ke type esh "rc.api.garndma" ast
                    new string[]
                    {
                        "rc.api.garndma"
                    })
                #region IdentityServerTutorialByChandraPrakashVariyani Project
                //new ApiResource("myresourceapi", "My Resource API")
                //{
                //    Scopes = {
                //        // dar noskheye 4.0.0 az identityserver dige in "Scope" ra nadarim va az noskheye 3.1.2 estefade shode
                //        new Scope("apiscope")
                //    }
                //}
                #endregion
            };

        public static IEnumerable<Client> GetClients() =>
            new List<Client>
            {
                new Client {
                    ClientId = "client_id",
                    ClientSecrets = {
                        new Secret("client_secret".ToSha256())
                    },

                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    
                    // dar startup e projeye ApiOne darim: config.Audience = "ApiOne"
                    AllowedScopes = { "ApiOne" }
                },
                new Client {
                    ClientId = "client_id_mvc",
                    ClientSecrets = {
                        new Secret("client_secret_mvc".ToSha256())
                    },

                    AllowedGrantTypes = GrantTypes.Code,

                    // taeine masiri ke age be jaei dastresi nadashim, redirect beshim ke dar einja be projeye MvcClient ham eshare dashtim
                    // dar amal age be safeei dastresi nadashte bashim, ebteda be account/login e projeye identity server montaghel mishim
                    RedirectUris = { "https://localhost:44361/signin-oidc" },
                    PostLogoutRedirectUris = { "https://localhost:44361/Home/Index" },

                    // dar startup e projeye ApiOne va ApiTwo darim: config.Audience = "ApiOne" and "ApiTwo" 
                    AllowedScopes = {
                        "ApiOne",
                        "ApiTwo",
                        IdentityServerConstants.StandardScopes.OpenId,
                        //IdentityServerConstants.StandardScopes.Profile,
                        // ye IdentityResource e jadidi eijad kardim ke name esh "rc.scope" ast
                        "rc.scope"
                    },

                    // puts all the claims in the id token
                    //AlwaysIncludeUserClaimsInIdToken = true,
                    AllowOfflineAccess = true,
                    // jahate inke bad az login, dige be consent naravim
                    RequireConsent = false,
                },
                new Client {
                    ClientId = "client_id_js",

                    AllowedGrantTypes = GrantTypes.Implicit,
                    
                    // addresse projeye JavascriptClient
                    RedirectUris = { "https://localhost:44387/home/signin" },
                    PostLogoutRedirectUris = { "https://localhost:44387/Home/Index" },
                    AllowedCorsOrigins = { "https://localhost:44387" },

                    // barmigarde be meghdari ke baraye scope dar file sign-in.js ba meghdare "openid ApiOne" dar nazar gereftim
                    AllowedScopes = {
                        IdentityServerConstants.StandardScopes.OpenId,
                        "ApiOne",
                        "ApiTwo",
                        "rc.scope"
                    },

                    // 1 second
                    AccessTokenLifetime = 1,

                    AllowAccessTokensViaBrowser = true,
                    RequireConsent = false,
                }
            };
    }
}
