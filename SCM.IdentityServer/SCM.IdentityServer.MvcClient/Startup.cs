using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace SCM.IdentityServer.MvcClient
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            #region Security Project => Authorization Configure - Add Policy
            //services.AddAuthorization(options =>
            //{
            //options.AddPolicy(
            //    "FacultyOnly",
            //    policy => policy.RequireClaim("FacultyNumber")
            //    //policy => policy.RequireClaim("roleType", "CanUpdatedata")
            //);
            //});
            #endregion

            services.AddAuthentication(config => {
                config.DefaultScheme = "Cookie";
                #region Security Project
                //config.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                #endregion
                config.DefaultChallengeScheme = "oidc";
            })
            #region Security Project
                //.AddCookie(options =>
                //{
                //    // jahate taeine zamane monghazi shodane cookie
                //    options.ExpireTimeSpan = TimeSpan.FromMinutes(60);
                //    // taeine ye esme delkhah ke dar edame khahim did ke baraye signout (Account/Logout) az in esm estefade mikonim
                //    options.Cookie.Name = "Cookie";
                //})
            #endregion
                .AddCookie("Cookie")
                .AddOpenIdConnect("oidc", config => {
                    // address e projeye identityServer
                    config.Authority = "https://localhost:44353/";
                    #region Security Project
                    //// jahate inke https ejbari nabashe
                    //config.RequireHttpsMetadata = false;
                    #endregion
                    // ClientId va ClientSecret tarif shode dar configure e projeye identityServer
                    config.ClientId = "client_id_mvc";
                    config.ClientSecret = "client_secret_mvc";
                    config.SaveTokens = true;
                    config.ResponseType = "code";
                    config.SignedOutCallbackPath = "/Home/Index";

                    // configure cookie claim mapping
                    config.ClaimActions.DeleteClaim("amr");
                    config.ClaimActions.DeleteClaim("s_hash");
                    // ye IdentityResource e jadidi eijad kardim ke UserClaims esh "rc.garndma" ast
                    config.ClaimActions.MapUniqueJsonKey("RawCoding.Grandma", "rc.garndma");

                    // two trips to load claims in to the cookie
                    // but the id token is smaller !
                    config.GetClaimsFromUserInfoEndpoint = true;

                    // configure scope
                    config.Scope.Clear();
                    config.Scope.Add("openid");
                    // dar Configuration.cs, dar AllowedScopes in "rc.scope" va "ApiOne" va "ApiTwo" ra darim
                    config.Scope.Add("rc.scope");
                    config.Scope.Add("ApiOne");
                    config.Scope.Add("ApiTwo");
                    config.Scope.Add("offline_access");
                });

            services.AddHttpClient();

            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapGet("/", async context =>
                //{
                //    await context.Response.WriteAsync("Hello World!");
                //});
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}
