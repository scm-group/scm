﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.AspNetCore.Mvc;

namespace SCM.IdentityServer.ApiTwo.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public HomeController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        [Route("/home")]
        public async Task<IActionResult> Index()
        {
            //retrieve access token
            var serverClient = _httpClientFactory.CreateClient();

            // address e projeye identityServer
            var discoveryDocument = await serverClient.GetDiscoveryDocumentAsync("https://localhost:44353/");

            var tokenResponse = await serverClient.RequestClientCredentialsTokenAsync(
                new ClientCredentialsTokenRequest
                {
                    Address = discoveryDocument.TokenEndpoint,

                    // ClientId va ClientSecret ei ke baraye ye client haye moshakhasi ke dar configure e projeye IdentityServer taein karde budim
                    ClientId = "client_id",
                    ClientSecret = "client_secret",

                    // dar startup e projeye ApiOne darim: config.Audience = "ApiOne"
                    Scope = "ApiOne",
                });

            //retrieve secret data
            var apiClient = _httpClientFactory.CreateClient();

            apiClient.SetBearerToken(tokenResponse.AccessToken);

            // address e projeye ApiOne
            var response = await apiClient.GetAsync("https://localhost:44361/secret");

            var content = await response.Content.ReadAsStringAsync();

            return Ok(new
            {
                access_token = tokenResponse.AccessToken,
                message = content,
            });
        }
    }
}