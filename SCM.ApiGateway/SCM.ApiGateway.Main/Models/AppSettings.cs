﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace SCM.ApiGateway.Main.Models
{
    public partial class AppSettings
    {
        [JsonProperty("Logging")]
        public Logging Logging { get; set; }

        [JsonProperty("Serilog")]
        public Serilog Serilog { get; set; }

        [JsonProperty("SkipHTTPS")]
        public bool SkipHttps { get; set; }

        [JsonProperty("AllowedHosts")]
        public string AllowedHosts { get; set; }

        [JsonProperty("GatewayName")]
        public string GatewayName { get; set; }

        [JsonProperty("Routes")]
        public IList<Route> Routes { get; set; }

        [JsonProperty("GlobalConfiguration")]
        public GlobalConfiguration GlobalConfiguration { get; set; }

        public IEnumerable<IConfigurationSection> GetChildren()
        {
            throw new System.NotImplementedException();
        }

        public IChangeToken GetReloadToken()
        {
            throw new System.NotImplementedException();
        }

        public IConfigurationSection GetSection(string key)
        {
            throw new System.NotImplementedException();
        }
    }

    public partial class Logging
    {
        [JsonProperty("IncludeScopes")]
        public bool IncludeScopes { get; set; }

        [JsonProperty("LogLevel")]
        public LogLevel LogLevel { get; set; }
    }

    public partial class LogLevel
    {
        [JsonProperty("Default")]
        public string Default { get; set; }

        [JsonProperty("Microsoft")]
        public string Microsoft { get; set; }

        [JsonProperty("Microsoft.Hosting.Lifetime")]
        public string MicrosoftHostingLifetime { get; set; }

        [JsonProperty("System")]
        public string System { get; set; }
    }

    public partial class Serilog
    {
        [JsonProperty("MinimumLevel")]
        public MinimumLevel MinimumLevel { get; set; }

        [JsonProperty("Enrich")]
        public List<string> Enrich { get; set; }

        [JsonProperty("LogFileName")]
        public string LogFileName { get; set; }
    }

    public partial class MinimumLevel
    {
        [JsonProperty("Default")]
        public string Default { get; set; }

        [JsonProperty("Override")]
        public Override Override { get; set; }
    }

    public partial class Override
    {
        [JsonProperty("Microsoft")]
        public string Microsoft { get; set; }

        [JsonProperty("System")]
        public string System { get; set; }
    }

    public class DownstreamHostAndPort
    {

        [JsonProperty("Host")]
        public string Host { get; set; }

        [JsonProperty("Port")]
        public int Port { get; set; }
    }

    public class AuthenticationOptions
    {

        [JsonProperty("AuthenticationProviderKey")]
        public string AuthenticationProviderKey { get; set; }

        [JsonProperty("AllowedScopes")]
        public IList<object> AllowedScopes { get; set; }
    }

    public class Route
    {

        [JsonProperty("DownstreamPathTemplate")]
        public string DownstreamPathTemplate { get; set; }

        [JsonProperty("DownstreamScheme")]
        public string DownstreamScheme { get; set; }

        [JsonProperty("DownstreamHostAndPorts")]
        public IList<DownstreamHostAndPort> DownstreamHostAndPorts { get; set; }

        [JsonProperty("UpstreamPathTemplate")]
        public string UpstreamPathTemplate { get; set; }

        [JsonProperty("UpstreamHttpMethod")]
        public IList<string> UpstreamHttpMethod { get; set; }

        [JsonProperty("AuthenticationOptions")]
        public AuthenticationOptions AuthenticationOptions { get; set; }

        [JsonProperty("QoSOptions")]
        public QoSOptions QoSOptions { get; set; }
    }

    public class QoSOptions
    {
        [JsonProperty("TimeoutValue")]
        public int TimeoutValue { get; set; }
    }

    public class GlobalConfiguration
    {

        [JsonProperty("RequestIdKey")]
        public string RequestIdKey { get; set; }

        [JsonProperty("AdministrationPath")]
        public string AdministrationPath { get; set; }

        [JsonProperty("BaseUrl")]
        public string BaseUrl { get; set; }
    }
}
