﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SCM.ApiGateway.Main.Models;
using System;

namespace SCM.ApiGateway.Main.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HealthCheckController: ControllerBase
    {
        private readonly AppSettings _appSettings;

        public HealthCheckController(IOptions<AppSettings> options)
        {
            _appSettings = options.Value;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return Ok(new 
            { 
                RequestTime = DateTime.Now, 
                Status = "Ok", 
                Desc = $"{_appSettings.GatewayName} as a gateway is up and running."
            });
        }
    }
}
