using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace SCM.ApiGateway.Main
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            var builder = Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

            builder.ConfigureServices(s => s.AddSingleton(builder))
                .ConfigureAppConfiguration(f =>
                {
                    f.AddJsonFile("configuration.json", optional: false, reloadOnChange: true);
                    f.AddJsonFile("appsettings.json", false, true);
                    f.AddJsonFile($"appsettings.Development.json", true, true);
                    f.AddEnvironmentVariables();
                });

            return builder;
        }
    }
}
